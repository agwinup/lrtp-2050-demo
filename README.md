Example [Hugo](https://gohugo.io) website using [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ce/user/project/pages/).

The theme used on this website is [Unicef Inventory](https://github.com/unicef/inventory-hugo-theme).

More information about this theme can be found at [this page](https://themes.gohugo.io/themes/inventory-hugo-theme/)

## Prerequisites

Make sure to read the [Web-based Plans page](https://wiki.ccrpc.org/pcd/web-plan) on the ccrpc.org wiki to understand how hugo works and what you should already be knowing before working on this project.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.

1. Navigate to your project's root directory

1. Preview your project locally by running the following command in a terminal window:

   ```shell
   hugo server
   ```

1. Add content to the content folder.

1. Make sure that your changes are reflected on the website by using:

   ```shell
   hugo server

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).