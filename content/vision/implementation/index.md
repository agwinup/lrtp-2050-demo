---
title: "Implementation"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 70
bannerHeading: Implementation
bannerText: >
  Implementing the LRTP is not only about completing the projects identified
  in the plan but also taking steps to incorporate the broader goals,
  objectives, and strategies.
---

The LRTP 2045 Vision is about more than just transportation. Public input
reinforces the idea that the transportation network is intricately tied to many
other conditions in the community including land-use, public health, the
environment, and the economy. The overall built environment operates most
effectively when all these different processes work together to facilitate
safe and efficient mobility to support our individual and collective
goals.

Plan implementation involves certain standard routine tasks that can be
considered on two levels: project-related implementation, and concept-related
implementation.

## Project-Related Tasks

### Review Project Priorities Periodically

Projects in the LRTP have undergone an initial prioritization process; this
prioritization should be reviewed periodically to include new projects and
change priorities if new funding or information becomes available. Two existing
documents can serve this purpose at the regional level: the [Transportation
Improvement Program (TIP)]( https://ccrpc.org/documents/tip-2020-2023/) and the
[Project Priority Review Guidelines(PPR)](
https://ccrpc.org/documents/project-priority-review-guidelines/).

The TIP is updated every year and lists transportation projects anticipated to
be constructed in the metropolitan planning area during the next four years. The
program reflects the goals, objectives, and recommendations from the 25-year
Long Range Transportation Plan in the short term. By mandate, the TIP must
include projects receiving federal and state funding; the
[CUUATS](https://ccrpc.org/programs/transportation/) program also includes local
projects from our member agencies. Compiling the document requires a consensus
among all [CUUATS](https://ccrpc.org/programs/transportation/) agencies, thus
establishing compliance with federal laws and eligibility for federal funding.

The [PPR
Guidelines](https://ccrpc.org/documents/project-priority-review-guidelines/)
outline the process by which
[CUUATS](https://ccrpc.org/programs/transportation/) evaluates and documents
consistency between the local use of federal Surface Transportation Block Grant
Program (STBGP) funds and federal and regional transportation goals as
established in the LRTP. Local agencies seeking to use STBGP funds are required
to submit a project application to be reviewed by CUUATS staff and the Project
Priority Review working group who use a set of criteria based on federal and
regional transportation goals to score each project. The project scores are
intended to illustrate the level of consistency with federal and regional
transportation goals and also serve as a way to measure projects against one
another in the event that multiple projects apply for STBGP funding in the same
year.

### Seek New Funding Resources

Local agencies should continually seek new funding sources for those projects
that do not currently have funding. When the new federal or state transportation
bill is enacted, local agencies should determine potential funding sources and
pursue projects that fit the intent of the new bill.

## Concept-Related Tasks

Complete Benchmarks from Plan  Performance Measures are based on the LRTP goals
and objectives as well as the project lists; they are measurable events that
signify progress and/or lack of progress toward defined goals. Every effort
should be made to update data sets related to the Performance Measures and
produce the LRTP Report Card each year.

### Monitor Area Development

While the LRTP is based on local agencies’ most available knowledge at the time
of writing, many new developments may occur while other anticipated developments
may not occur on time or at all. Decision making processes and future updates to
the plan should reflect these changes.

### Evaluate Change

Proactive planning should be utilized to anticipate changes to the
transportation network. Where unanticipated changes occur, local agencies should
assess how unanticipated changes will affect the transportation system, and
react to those changes logically and efficiently.

### Update Models

The [CUUATS Modeling Suite](https://ccrpc.gitlab.io/lrtp2045/data/models/) must
be updated as continually as staff and funding permit. Updated traffic volumes,
population forecasts, employment forecasts, and other inputs are essential
elements to have accurate working models. Seek Funding for Implementation Some
of the goals and objectives will require significant staff time or other inputs
to be completed. Funding must be sought for both transportation projects and
concepts implementation.
