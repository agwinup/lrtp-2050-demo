---
title: "Funding"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 60
bannerHeading: Funding
bannerText: >
  The 25-year vision for the urbanized area transportation system is defined,
  in part, by how much money there is to fund the infrastructure projects
  needed to realize it.
---
[CUUATS](https://ccrpc.org/programs/transportation/) staff establishes the
25-year vision for the Metropolitan Planning Area (MPA) transportation system to
correspond with available funding for future infrastructure projects. The
proposed future projects in the LRTP 2045 align with collective [transportation
goals](https://ccrpc.gitlab.io/lrtp2045/vision/goals/) for the region, such as
reducing emissions, crashes, and the number of roads and bridges in poor
condition. This chapter will outline existing revenue sources and expenditures
for the transportation system as well as document how staff and local agencies
project revenues for the 25-year planning horizon.

Predicting future revenues is a somewhat elusive exercise, yet there are
guidelines to be followed. By federal law, the LRTP must include a financial
plan that demonstrates how the adopted transportation plan can be implemented.
In order to address this requirement and provide transparency about project
implementation, the [future transportation
projects](https://ccrpc.gitlab.io/lrtp2045/vision/vision/) listed in this plan
are labeled according to whether or not they have secured funding prior to this
plan being approved. In addition, this chapter will include additional
information on the agencies and federal funding sources that directly impact the
transportation system in the Metropolitan Planning Area (MPA).

There are two basic categories for transportation spending:

1. Operations and Maintenance – keeping the existing transportation
infrastructure in good operational condition through resurfacing,
reconstruction, and repairing as periodically needed.

2. Expansion – creating new transportation infrastructure to increase access for
additional transportation modes or for all modes to additional locations.

Securing funding for expansion projects requires
increasing the operations and maintenance budget to accommodate the ongoing
upkeep of additional transportation infrastructure. To effectively choose where
and how roadways are resurfaced, upgraded, or expanded, transportation officials
must use asset management practices, such as life cycle cost analyses, to
maximize transportation funding resources, including taxpayer dollars.

## Revenues ##

Much of the transportation funding at the local, state, and federal level relies
on the Motor Fuel Tax (MFT) revenue system. The MFT is a user-based fee where an
excise tax is imposed on the sale of motor fuel in order to fund transportation
system maintenance and improvements for motor vehicles. MFT can be collected at
the federal, state, and local levels. Unfortunately, MFT is not currently
sufficient to support the operations, maintenance, and expansion of today’s
transportation system.

Under current federal law, the federal MFT rate is 18.3 cents per gallon on
gasoline and 24.3 cents per gallon on diesel fuel. The federal MFT hasn’t been
increased to keep up with inflation or increasing construction costs since 1993
In the past two decades, motor vehicles have increased their fuel efficiency,
meaning drivers can operate their vehicles on increasingly smaller amounts of
fuel. To begin to address these funding shortfalls, in 2012 both the Cities of
Champaign and Urbana imposed an additional $0.04/gallon Motor Fuel Tax at the
local level.

Fortunately, a new state bill passed in 2019, Rebuild Illinois will double the
state MFT from $0.19 to $0.38. Of the increased state MFT revenue, 32 percent
will go to units of local government through the motor fuel tax formula and
another 20 percent will go to local transit districts. Until this bill is fully
implemented, the impacts of the additional MFT financing are largely unknown.

In addition to MFT, there are many different strategies being tested around the
country to address improve transportation infrastructure funding, including
mileage-based user fees, local sales taxes, public-private partnerships,
infrastructure banks, short-term federal discretionary grants, and additional
toll roads. It is unknown at this time if any of these revenue sources will
eventually replace MFT revenues at the local, state, and/or federal levels.

### Federal ###

On December 4, 2015, President Obama signed into law the [Fixing America’s
Surface Transportation Act (FAST Act)](https://www.fhwa.dot.gov/fastact/). The
FAST Act funds surface transportation programs – which include, but are not
limited to, Federal-aid highways – at over $305 billion for fiscal years (FY)
2016 to 2020. The FAST Act builds on the existing transportation bill passed in
2012, [Moving Ahead for Progress in the 21st Century
(MAP-21)](https://www.fhwa.dot.gov/map21/), which supports the maintenance of
the country’s existing transportation infrastructure and guides future growth
and development of the transportation network. The Federal Highway
Administration works with local stakeholders to ensure that individual
communities are able to both maintain and construct safe, multimodal,
sustainable transportation projects ranging from passenger rail and public
transportation infrastructure to bicycle and pedestrian facilities.

Federal transportation grants require different levels of local matching funds,
ranging from 10 percent to 50 percent. The FAST Act authorizes a single amount each year for
all the apportioned highway programs combined, as dictated by MAP-21.  This
includes the [National Highway Performance Program
(NHPP)](https://www.fhwa.dot.gov/specialfunding/nhpp/), the [Surface
Transportation Block Grant Program
(STBG)](https://www.fhwa.dot.gov/specialfunding/stp/) (formerly Surface
Transportation Program), [Highway Safety Improvement Program
(HSIP)](https://safety.fhwa.dot.gov/hsip/) (including Railway-Highway
Crossings), [Congestion Mitigation and Air Quality Improvement Program
(CMAQ)](https://www.fhwa.dot.gov/fastact/factsheets/cmaqfs.cfm), and
[Metropolitan
Planning](https://www.fhwa.dot.gov/fastact/factsheets/metropolitanplanningfs.cfm),
plus a newer [National Highway Freight Program
(NHFP)](https://www.fhwa.dot.gov/fastact/factsheets/nhfpfs.cfm).

### State ###

On June 1, 2019, Illinois legislators passed [Rebuild
Illinois](https://www2.illinois.gov/IISNews/20266-Rebuild_Illinois_Capital_Plan.pdf),
a six-year infrastructure bill that includes $44.8 billion invested over six
years, of which $33.2 billion are dedicated to transportation across all modes.
The bill includes $10.44 billion in new state level funding that will maintain,
enhance, and upgrade Illinois’ highway network. It also includes both a transit
bonding component and a new state source recurring revenue for ongoing capital
funding totaling $4.5 billion over the life of the six-year program: $3 billion
in new bond funding and $1.5 billion in new recurring revenue for mass transit
throughout the state, of which downstate transit districts will receive $300
million in bond proceeds and $150 million in recurring revenue to maintain and
improve their systems. Rebuild Illinois also includes a $3.9 billion increase in
direct funding for roads and bridges to municipalities through state bonding
($1.5 billion) and additional MFT revenues ($2.4 billion). A portion of state
MFT revenues are distributed by IDOT to counties, townships, and municipalities
for use in transportation-related expenditures. The MFT revenue increase from
$0.19 to $0.38 in 2019 will continue as an ongoing source of income for the
local roads system into the future.

The Illinois Department of Transportation (IDOT) receives funding from the state
to maintain its highways and is also charged with distributing federal funding
from the aforementioned MAP-21 Programs to the state Metropolitan Planning
Organizations (MPO). The allocation for each MPO is based on urbanized area
population. Most state funding allocated to projects in the urbanized area
require at least a 20 percent local match from an MPO member agency or outside source
such as a private party. The Illinois Department of Natural Resources (IDNR)
also provides funds for greenways and trails projects with a required local
match. Many local bike paths and trails have been funded, in part, by IDNR.

### Local ###

Transportation projects receive local funding through
municipal and county budgets, local MFT, public transit fares, local park
district budgets (for greenways and trails projects), the University of
Illinois, and private donations.

## Forecasting Revenues, 2020-2045 ##

What follows is a summary of
[CUUATS’](https://ccrpc.org/programs/transportation/) methodology for estimating
revenues for the 25-year planning horizon on a source-by-source basis. As with
most projections about the future, uncertainty increases as the planning horizon
moves further out. Many factors affect transportation projects, which makes the
determination for funding complex at the federal, state, and local levels. The
funding projections shown in this chapter will be based on projects documented
in past and current versions of the local Transportation Improvement Program
(TIP) as well as future growth rates provided by the different agencies
involved.

### Federal ###

Federal funding fluctuates annually by millions of dollars. In order to provide
a reasonable estimate, four different calculations were completed based on
varying program funds:

- All federal program funds, with the exception of STBGP/STPU Local dollars
  and transit funding, were calculated for the FY 2020-23 Transportation
  Improvement Program (TIP). The estimated funding for 2020-2023 was
  $202,291,818.

- To estimate the remaining 21 years of federal funding, the average was taken
  of the previous five years’ (FY 2015 - 2019) of federal funding allocations,
  not including the STP-U Local and transit apportionment, as documented in the
  annual listing of Federally Obligated Projects. A 4 percent annual growth rate
  was applied to the annual average and projected to 2045, which produced an
  estimate for federal funding for 2024 - 2045 of $547,207,310.

- STP-U Local funding was estimated by applying a 4 percent annual growth rate
  to the FY2020 allocation of ($1,615, 236.95 for Champaign-Urbana and
  $101,651.92 for Mahomet) through 2045 for a total of $76,078,341.

- The Champaign-Urbana Mass Transit District estimates there will be
  $100,000,000 available in the next 25 years through FTA Formula Capital funds,
  which are used for fleet expansion and replacement.

- The Champaign-Urbana Mass Transit District was awarded a $17,275,000 grant
  from the U.S. Department of Transportation's Buses and Bus Facilities Grant
  Program for the Illinois Terminal Expansion Project scheduled to begin in
  Fiscal Year 2020 or 2021.

**2020-2045 total for all Federal programs: $942,852,469**  

### State ###

The forecast for future state funding is composed of three steps: total annual
state-only funds allocated for projects in the FY 2020-2023 TIP, projected
state funds available for FY 24-45 based on the FY2015-2019 average, and
total annual funds allocated for projects in the TIP FY 20-23 by the Illinois
Department of Natural Resources. Although the state recently passed a six-year
infrastructure bill, [Rebuild
Illinois](https://www2.illinois.gov/IISNews/20266-Rebuild_Illinois_Capital_Plan.pdf),
that includes $33.2 billion in transportation, at this time it is unclear how,
where, and when any of that money will be invested at the local level.

- Total of state-only funds allocated for projects in TIP FY 2020-2023 is $8,273,000.

- To estimate the remaining 21 years of state funding, the average was taken
  of the previous five years’ (FY 2015 - 2019) of state funding allocations, not
  including transit funding, as documented in the annual TIP documents, with an
  attempt to eliminate funding redundancies of projects that carryover year to
  year. A 4 percent annual growth rate was applied to the annual average and
  projected to 2045, which produced an estimate for state funding for 2024 -
  2045 of $233,621,151.

- Funding from the Illinois Department of Natural Resources is not a fixed
  part of the annual budget; rather, it is based on the approval of intermittent
  grant applications. The only reasonably guaranteed funding from this source is
  that which would be listed in the current TIP, however no IDNR funding is
  listed in the current TIP.

**2020-2045 total for all State programs: $241,894,151**  

### Local ###

To estimate local transportation revenues from 2020-2045, each local agency
provided local budget projections including motor fuel tax revenues estimates
and future growth rates, when possible. Local budget estimates are limited to
funding spent for transportation improvements, operations, and maintenance.

#### City of Champaign ####

The City of Champaign provided estimates for revenues through 2030 and
projections that reflect an annual growth rate of approximately 0.5 percent
between 2030 and 2045 for an estimated total transportation revenue of
$233,426,138.

#### City of Urbana ####

The City of Urbana provided estimates for revenues through 2021 and projections
that reflect an annual overall growth rate of approximately 0.5 percent between
2022 and 2045 for an estimated total transportation revenue of $93,231,495.

#### Village of Savoy ####

The Village of Savoy provided an estimate for revenues through 2045 that
includes an annual growth rate of 0.5 percent between 2023 and 2045 for an
estimated total transportation revenue of $13,167,338.

#### Village of Mahomet ####

The Village of Mahomet provided an estimate for revenues through 2045 that does
not include an annual growth rate for an estimated total transportation revenue
of $15,304,034.

#### Village of Tolono ####

The Village of Tolono provided estimates for revenues through 2045 that do not
include any annual growth rates for an estimated total transportation revenue of
$2,210,000.

#### Village of Bondville ####

The Village of Bondville does not have any official plans related to
transportation funding, therefore no reasonably guaranteed funding is
anticipated.

#### Champaign County ####

Champaign County contributions within the MPA are on a project-by-project basis.
Only those projects that have established agreements with the County for MPA
transportation projects, as documented in the FY 2020-2023 TIP can be considered
reasonably guaranteed. At this time, estimated funding from the County through
2045 is $2,347,222.

#### MTD ####

The Champaign-Urbana Mass Transit District receives an allocation from the state
and uses its local revenues to cover operations and maintenance for its system.
The Champaign-Urbana Mass Transit District provided projections for local and
state operating revenues through 2045 that reflect an annual growth rate of
approximately 5 percent for an estimated total of $1,818,000,000.

#### University ####

The University of Illinois has no reasonably guaranteed funds for transportation
improvements or maintenance with the exception of what is listed in the FY
2020-2023 TIP. At this time, estimated transportation funding from the
University through 2045 is $3,373,523.

#### Willard Airport ####

The University-owned Willard Airport in Savoy reported $3,000,000 in revenues
for FY 2019. Staff at the University was unable to provide a formal projection
to the year 2045, so $3,000,000 was assumed to be the annual budget each year
from 2020 through 2045 for a total of $78,000,000.

#### Private Sources ####

The only reasonably guaranteed private transportation funds are found in the FY
2020-2023 TIP, where there are currently two projects that are partially funded
with private funds from Carle Health System totaling $29,119.

**2020-2045 total for all local programs: $2,259,088,869**  

### Forecasting Expenditures, 2020-2045

To operate, maintain, and expand our local area transportation network, local
agencies project they will spend all future revenues through 2045. Without future
projections for state and federal expenditures, the only state and federal
expenditures reflected in the table below are those documented in the current
TIP FY2020-2023.

#### Summary of Revenue and Expenditure Projections, 2020-2045  

<rpc-table url="future_funding.csv"
  text-alignment="c,c"></rpc-table>
