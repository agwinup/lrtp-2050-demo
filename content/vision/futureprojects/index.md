---
title: "Future Projects"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 30
bannerHeading: Future Projects
bannerText: >
  This chapter sets forth a vision for each mode of transportation in the
  urbanized area through the year 2040.
---

CUUATS staff used [public input from the first round of LRTP 2045
outreach](https://ccrpc.gitlab.io/lrtp2045/process/round-one/), input from local
agencies, and local planning documents to establish the LRTP 2045 goals (safety,
multimodal connectivity, economy, equity, and the environment) along with a list
of transportation system changes that could contribute to the realization of the
goals. Public input regarding the *current* transportation system called
attention to locations in the system that people thought were functioning well
and locations where people suggested specific infrastructure improvements.
Public input regarding the *future* transportation system came from responses to
a question asking people to prioritize the three most important factors that
will shape our transportation system over the next 25 years. The combined input
about the current and future transportation system conveyed strong public
support for a set of overlapping ideas about the future of transportation: a
more environmentally sustainable transportation system, additional pedestrian
and bicycle infrastructure, shorter off-campus transit times, equitable access
to transportation services, and a compact urban area that supports active
transportation and limits sprawl development.

## Future Projects: Regionally Significant Vision Projects ##

The graphic below highlights the overarching transportation goals for the future
alongside regionally-significant transportation projects as part of the LRTP
2045 future vision. The regionally-significant projects and collectively-defined
goals are connected in the graphic to illustrate how individual improvement
projects can complement other projects to improve the overall system over time.
Some projects are already funded and in progress while others are considered an
illustrative, un-funded part of the LRTP vision for 2045.

{{<image src="LRTP Vision Board_22x34_reduced.jpg"
  alt="Chart of LRTP 2045 goals listed with regionally significant vision projects and an illustrative map"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

  The intent of the following vision video is to get people thinking about the future and imagining different changes in the local community in 2045. This video is not intended to be a comprehensive or literal representation of the LRTP 2045.
  <iframe width="100%" height="315" src="https://www.youtube.com/embed/MwJMs9c31dc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Future Projects: Fiscally Constrained ##

As part of the LRTP, the Federal Highway Administration requires a listing of
the fiscally constrained projects that are part of the overall vision for the
urbanized area. The fiscally constrained projects are those that have either
guaranteed or reasonably guaranteed funding secured for the completion of the
project. A separate federally-required document for the region, the
Transportation Improvement Program (TIP) lists fiscally-constrained
transportation projects anticipated to be constructed in the metropolitan
planning area during the next four years. The program reflects the goals,
objectives, and recommendations from the 25-year Long Range Transportation Plan
(LRTP) in the short term. By mandate, the TIP must include projects receiving
federal and state funding; the CUUATS TIP also includes local projects from our
member agencies. Compiling the document requires a consensus among all CUUATS
agencies, thus establishing compliance with federal laws and eligibility for
federal funding. Each year, the Illinois Department of Transportation (IDOT)
publishes a fiscally-constrained six-year program that details transportation
investments in the state and local highway system. As of September 2019, the
State of Illinois has not yet released the state’s multi-year transportation
improvement program due to the pending implementation of the Capital Bill
approved in July 2019. The local TIP will be amended to reflect additional
State-funded transportation projects as they are released. The following table
includes a summary of local projects reflected in the current TIP for fiscal
years 2020-2023. Visit the online [TIP map and
database](https://maps.ccrpc.org/tip-2020-2023/) for more details about these
projects and other transportation expenditures in the region.

<rpc-table url="TIP summary 20191008.csv"
  table-title="CUUATS TIP Summary"
  text-alignment="c,c"></rpc-table>

Furthermore, the City of Champaign, City of Urbana, Village of Savoy, and
Village of Mahomet identify additional infrastructure maintenance and
improvement projects in their respective Capital Improvement Plans and Programs.

## Future Projects: Local and Unfunded ##

Each agency in the region has its own set of transportation priorities and goals
to improve mobility in their respective jurisdiction alone and in conjunction
with surrounding jurisdictions. The following list includes local project
priorities for the future that are currently un-funded. The projects are
numbered in the map for identification purposes only and do not indicate
priority.

<iframe src="https://maps.ccrpc.org/lrtp2045-roadway-vision-projects/"
  width="100%" height="600" allowfullscreen="true">
  Map showing roadway vision projects.
</iframe>

<rpc-table url="localunfunded_numbered.csv"
  table-title="Future Projects: Local and Unfunded"
  text-alignment="c,c"></rpc-table>

Bicycling and walking projects were identified separately from roadway projects
due to their large volume and small scale. The recommendations from the
following plans were incorporated into the map below:

- 2017 Champaign Park District Trails Master Plan (CTMP) – approved June 2017
- 2017 Savoy Bike & Pedestrian Plan – approved April 2017
- 2016 Urbana Bicycle Master Plan (UBMP) – approved December 2016
- 2016 Urbana Park District Trails Master Plan (UTMP) – approved January 2016
- 2014 Champaign County Greenways & Trails Plan – approved June 2014

<iframe src="https://maps.ccrpc.org/lrtp2045-bicycle-pedestrian-vision/"
  width="100%" height="600" allowfullscreen="true">
  Map of the future pedestrian, bike, and trail projects included in 2017 Champaign Park District Trails Master Plan (CTMP), 2017 Savoy Bike & Pedestrian Plan, 2016 Urbana Bicycle Master Plan (UBMP), 2016 Urbana Park District Trails Master Plan (UTMP), and 2014 Champaign County Greenways & Trails Plan.
</iframe>

The implementation of the local projects described in the following tables are
not the responsibility of CUUATS or the Champaign County Regional Planning
Commission. These projects have been included in the LRTP to highlight local
improvement projects that will enhance the quality of the overall transportation
network and will help connect the local transportation network to the planned
regionally significant improvements for the future.



## From Freight Plan

### Future Projections ###
Just as the population in the metropolitan planning area will experience growth over time, the freight system within the Champaign-Urbana region will also see significant growth. The Champaign County roadway network is projected to experience an increase of nearly 23% in Average Daily Truck Trips (ADT) by 2045, increasing from 19,000 trips in 2015 to 24,000 trips in 2045. Additionally, total truck miles traveled in the MPA are projected to increase around 20 percent, causing the MPA's projected congested lane miles to more than double due to increased truck travel. Thus, the metropolitan planning area will experience an increase in freight use in the next twenty-five years that will require a safe, efficient transportation network that equally benefits truck and other modes of travel.  
 
#### Commodity ####
Not only will freight continue to grow, but the industry's role will also change with time. Currently, the top commodity types by tonnage are cereal grains, gravel, other foodstuffs, and other agricultural products. By 2045, these four commodities are projected to remain in the top, making up nearly 59 percent of total commodity tonnage and accounting for 64 percent of the increase in tonnage from 2017 to 2045. The only two top commodities to experience a decrease in tonnage transported from 2017 to 2045 were gasoline and fuel oils, which decreased by 6 and 12 percent respectively. This change seemingly reflects the shifts in fuel efficiency and vehicle engine types over the next couple of decades. 

The leading commodity types by value are mixed freight, other foodstuffs, and electronics. Combined, these three commodity types have a value of 9.1 million dollars and are projected to represent nearly 30 percent of the total truck commodity value in 2045. Electronics and Machinery are expected to show the largest growth in percentage of value from 2017 to 2045, both increasing by more than 85 percent in total value. Similar to its decrease in tonnage transported, gasoline is projected to decrease in total value by 8 percent from 2017 to 2045. 

#### Trading Partners ####
In 2045, the top 20 trading partners are projected to be very similar to those in 2017. The top five trading partners remain Iowa, Indiana, McLean County, Cook County (Chicago), accounting for more than 22 percent of all truck commodity tonnage in 2045. Additionally, the top 20 trading partners are projected to account for around half of all commodities entering or exiting Champaign County transported by trucks. The largest increase in total commodity tonnage traded with Champaign County by truck is projected to be the State of Iowa, an estimated 76 percent increase from 2017. As for value, the top five trading partners will continue to account for nearly 25 percent of the total truck commodity value in 2045. The State of Texas is projected to experience the largest increase in the total commodity value traded with Champaign County, with an increase of 85 percent. 

For more information on the region's freight and future projections, read the [Champaign Urbana Region Freight Plan](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/).
