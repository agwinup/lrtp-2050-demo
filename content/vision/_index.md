---
title: 2050 Vision
date: 2018-09-18T13:50:42-06:00
draft: true
menu: main
weight: 30
---

The combined input about the current and future transportation system conveyed
strong public support for a set of overlapping ideas about the future of
transportation: a more environmentally sustainable transportation system,
additional pedestrian and bicycle infrastructure, shorter off-campus transit
times, equitable access to transportation services, and a compact urban area
that supports active transportation and limits sprawl development.
