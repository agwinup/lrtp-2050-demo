---
title: "Demographics"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 10
bannerHeading: Demographics
bannerText: >
  The Champaign-Urbana Urbanized Area is a diverse region spanning 47 square
  miles and containing a population of more than 148,000 people in east-central
  Illinois.
---
**This page is in the process of being updated**

  *The Champaign-Urbana Urbanized Area spans approximately 47 square miles and
  contains a population of more than 148,000 people in east-central Illinois. The
  region is located 135 miles south of Chicago, Illinois, 120 miles west of
  Indianapolis, Indiana, and 180 miles northeast of Saint Louis, Missouri. Five
  municipalities are partially or wholly within the Champaign-Urbana Urbanized
  Area, including: City of Champaign, City of Urbana, Village of Savoy, Village of
  Tolono, and Village of Bondville. In contrast, the Metropolitan Planning Area
  (MPA) is the 25-year planning area for the long range transportation plan. The
  MPA spans 179 square miles and includes the Champaign-Urbana Urbanized Area, as
  well as some additional area surrounding the urbanized area including the
  Village of Mahomet. The function of the MPA boundary is to consider areas that
  could be developed and included in the urbanized area over the next 25 years.*


## Age and Gender 


## People with Disabilities


## Race and Ethnicity 


## Income and Poverty 


## Population Distribution and Density 



## Employment 

Champaign County is a regional employment center because of the presence of 
education, health care, and manufacturing employers in the area. The Census Bureau 2020 ACS 5-Year Estimates identified the following top five employments sectors for Champaign County in 2020: 

  1.  **Educational Services:** 26,823 workers 
  2.  **Health Care and Social Assistance:** 13,975 workers 
  3.  **Retail Trade:** 10,333 workers 
  4.  **Accommodation and Food Services:** 8,420 workers 
  5.  **Manufacturing:** 7,206 workers 


### Unemployment Rate

According to the Illinois Department of Employment Security (IDES), the annual average unemployment rate increased from 4.3% in 2017 to 6.6% in 2020, with the onset of the COVID-19 pandemic. Since then, unemployment rates are similar to those pre-pandemic. The following table shows Champaign County's unemployment rate from 2017 to 2022.  

 
 <rpc-chart url="Unemployment 2017-2022.csv"
  chart-title="Champaign County Unemployment, 2017-2022"
  x-label="Year"
  y-label="Unemployment Rate"
  type="line"
  source-url="https://ides.illinois.gov/resources/labor-market-information/laus.html" 
  source=" IDES, Accessed September 5, 2023"></rpc-chart>

 

### Top Employers in Champaign County

Champaign County's list of top employers has remained relatively consistent over the years. The most current imformation being from 2018, this section utilizes the Champaign County Regional Workforce Profile (2018) to determine the top employers and their total employees. The University of Illinois Urbana-Champaign is the county's largest employer, with nearly 15,000 employees. This is followed by Carle Foundation Hospital with an estimated 6,500 employees. The following table lists Champaign County’s top employers since 2018.
 
<rpc-table 
url="Top_Employers_2018.csv" 
text-alignment="1,r" 
table-title="Champaign County Top Employers (2018)" 
switch="false" 
source-url="http://www.champaigncountyedc.org/area-facts/directories-reports" 
source=" Champaign County Economic Development Corporation, 2018 Top Employers - Champaign County, Illinois, Accessed 7 September 2023"> 
</rpc-table> 
