---
title: "2020 Decennial Census"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 10
bannerHeading: 2020 Decennial Census
bannerText: >
  Every 10 years, as stipulated by the US Constitution, the federal government is required to count all the people residing in the United States. CUUATS and other transportation agencies rely on Census data to make educated determinations about transportation needs.
---

## The Long Range Transportation Plan and the 2020 US Census ##

Every 10 years, as stipulated by the US Constitution, the federal government is required to count all people residing in the United States. This count requires an immense amount of effort. Doing this helps to determine representation at the local, state, and federal levels. It also is used by CUUATS and other local transportation agencies to make determinations about transportation modeling, projects, and priorities. 
 
The 2020 Decennial Census demographic data presented here as part of the Long Range Transportation Plan 2050 is a snapshot in time when COVID restrictions were at their peak. This contrasts with the 2022 American Community Survey data, which is informed by the Decennial Census as well as other sample surveys that are taken by the Census Bureau in other years as it contains very comprehensive demographic data. 

The 2020 Census snapshot, however, appears to have unusual population counts that would not be considered normal for the area, i.e., population changes in the area due to changes caused by the COVID-19 pandemic. The largest issue taken with the 2020 census count is that there appears to be an undercount as many students and some faculty from the University of Illinois Urbana-Champaign were not in the area during 2020 due the campus being closed to in-person classes and other travel restrictions. Hence, CUUATS and other local agencies do not consider the 2020 Census numbers to be a reasonable representation of normal yearly population levels. This section details an explanation of those changes and other differences in the 2020 Census compared to previous counts. 

The LRTP 2050 will then present the census data and contextualize it in this section to show that the student-age population normally in Champaign-Urbana during the school year was underrepresented in its population counts. This can and will have effects on future grant opportunities until the next census and may also influence the future projects chosen for the LRTP. Conclusions are provided at the end of this section to show how CUUATS will be attempting to rectify issues with the data with the modelling scenarios completed as part of this document.
 
## Population Changes ## 
The total population change from the previous LRTP and the 2020 census shows potential descrepancies in the data. It should be noted for this comparison that the LRTP 2045 used 2017 5-year estimates from the ACS while the 2020 Census data is single-year data. Usually, decennial census data can be considered more accurate as it intends to collect responses from every individual in the nation, while the 5-year ACS data only takes a sample of the population and applies formulas to estimate the current population. This comparison, as seen in the table below, suggests that Champaign and Savoy gained a modest 3% and 3.6% in population, respectively. Contrastingly, Mahomet and Tolono made bigger gains at 15.5% and 24.3%, respectively.  Lastly, Bondville and Urbana appear to have lost a significant amount of population, 18.0% and 9.0%, respectively. Some of these changes appear reasonable while others do not. So, how does CUUATS interpret these results?

<rpc-table 
url="Population_Statistics.csv" 
text-alignment="1,r" 
table-title="Champaign-Urbana Population 2020 Census and 2017 ACS Data Comparison" 
switch="false" 
source="2020 Decennial Census Demographic and Housing Profile Data; LRTP 2045 Demographic Table Data"> 
</rpc-table> 

Based on local knowledge of the community, it was expected that the village of Bondville would lose population, however an 18% drop is quite considerable. For the Village of Tolono, the population was expected to increase, but a 24.3% increase is rather incredible as well. The Village of Mahomet has been experiencing a population boom over the last decade, and the 15.5% population increase would be within the reasonable means of increase based on year-over-year projections and the great increase in housing being built there. 

The most concerning population count is that from the City of Urbana. A 9% drop in population had not been noticed or felt by the city until the lack of students during the 2020 and 2021 academic years. The City and CUUATS are certain this drop in population is almost solely due to the lack of students being on campus during the decennial census count. Proximate population estimates derived for updated ACS data before the next census will likely not consider this error unless a recount of the population is completed. Unfortunately, a census recount is unlikely to be completed and approved until at least 2025, after the LRTP 2050 is completed and approved. 

Lastly, Champaign and Savoy both show reasonable population increases between 3% and 4%. However, looking more closely at specific data demographics, this reasonable population increase may actually have been far more substantial if it had not been for COVID-19's changes to the area population in 2020. Based on regional agency planning and knowledge, CUUATS will have to make educated recommendations on how to project population growth or decline in the area at the end of this section instead of relying upon the decennial data or the most current available ACS data.

## Age ##

Reviewing census counts by age providez some more insight on where counts may be misrepresented in the region. The charts below show the raw number counts taken during the 2020 Decennial Census. Hovering over any single bar in the chart will show the statistics for each age range. 

Upon inspecting these counts, Urbana's population not only decreased, but the decrease significantly comes from persons aged 15-24 years (nearly 4,300 people) while the population in Champaign grew significantly for the same age range (nearly 3,600 people). This could suggest some students that would have previously lived in Urbana left for Champaign as new housing was built. However, it does not account for all of the population change. <b> Additionally, the University has seen student counts grow year-over-year for some time. </b> It would be highly unexpected for fewer people of this age range not to be living in the area during the Census count.

Similarly, Savoy's population saw growth, but persons aged 15-24 still decreased, though by much less (109 fewer). Together, these numbers indicate a lack of growth where the area knew growth occurred. The difference, clearly, being the onset of COVID restrictions, reducing the student-age population in the area during the decennial count.

These anamolies in the population count could mean that local municipalities will request special census counts to get a more accurate picture of the current populations. These counts would not be completed until after the LRTP 2050 is published.

Other notable changes to the area's counts that will have long-term effects include Bondville's population decreasing and Tolono and Mahomet's significant population increases. Some of the increases look like they have outpaced the expected increases by the American Community Survey. Hence, this will have an effect on area planning and growth expectations over the next 25 years.

<rpc-chart
url="Total_Municipal_Population_2020_Census.csv"
type="bar"
stacked="true"
y-label="Total Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

<rpc-chart
url="Total_M_S_Population_2020_Census.csv"
type="bar"
stacked="true"
y-label="Total Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

<rpc-chart
url="Total_T_B_Population_2020_Census.csv"
type="bar"
stacked="true"
y-label="Total Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

## Race and Ethnicity ##

The racial and ethnic makeup of the MPA population remains more diverse than the nation overall. The 2020 Census count showed some consistency in that the percentage of minority racial groups in Champaign and Urbana have the highest rates of non-white populations with fairly similar percentages for all races in both cities. Black or African American populations constitute the largest minority, followed closely by the Asian populations of both cities. Each of these groups are anywhere from 16.5% to 19.0% of the total population. The Asian population makes up the largest minority in Savoy at around 18.4%.

<rpc-chart
url="Percent_Race_Pop.csv"
type="bar"
stacked="true"
y-label="Percent Population"
y-min="0"
y-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

Looking more closely at the 2020 Census numbers, the percentage of minority populations increased in all MPA municipalities since the 2017 numbers that were reported for the last LRTP (seen below). However, this was partially because of a change in methodology by the Census in how it reports race designations. In previous Census surveys, questions were separated on race and Hispanic ethnicity and were described separately. However, in the data on race for the 2020 Census, Hispanic origin was also considered for reporting population numbers for race and was coded as “Some Other Race” when indicated in the write-in portion of the question. Hence, there appears to have been a large increase in persons being reported as “Some Other Race” or as “Two or More Races”. 

It becomes quite clear that the change to the coding of the results from the 2020 decennial census has caused increased reporting of persons as Some Other Race or as Two of More Races. Looking at the percentage difference from the 2017 ACS data to the 2020 Decennial Census data, every municipality had an increase in the percentage of the population reported as Two or More Races, and most had an increase in those reported as Some Other Race, except for Savoy. It is clear that the change to the Census reporting changed the proportional reporting of different races. Comparisons of cities between 2017 ACS data and 2020 Census counts is shown in the charts below.

<rpc-chart
url="C_U_Race_Pop.csv"
type="bar"
stacked="true"
y-label="Percent Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

<rpc-chart
url="M_S_Race_Pop.csv"
type="bar"
stacked="true"
y-label="Percent Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

<rpc-chart
url="T_B_Race_Pop.csv"
type="bar"
stacked="true"
y-label="Percent Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2020 Decennial Census Estimates, Table DP01"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Population Change from 2017 ACS Estimates to 2020 Decennial Census"></rpc-chart>

Additionally, it is critical to discuss the change in the White population for the area. First, a large proportion of the increases to the Some Other Race and Two or More Races categories likely came proportionately from the White population. This means that in 2017, many of these people would have been considered White only. This would come directly from the change in the Census' coding of race, many of whom would be reporting Hispanic heritage, for which there previously was no race category.

Secondly, not all of the changes to these proportions are related to the change in the 2020 US Census. As you can see, the proportional percentage decreases in White population still outpace the increases in the Some Other Race and Two or More Races categories. This is because the absolute number of people from the White population decreased. From the 2017 ACS 5-year estimates to the Decennial Census, Champaign and Urbana show absolute decreases in population of close to 2,800 and 2,600 White people, respectively (See Appendix). This, at a glance, would appear very strange. However, we know that the COVID restrictions left many students learning from home. These changes to the population would not be unreasonable as the University hosts tens-of-thousands of students every academic year.Along with the knowledge that the population of students had been increasing over the last decade, the decrease may also be hiding what would have been a higher population increase for the area in the last decade.

<rpc-table 
url="Total_Pop_Diff_Race.csv" 
text-alignment="1,r" 
table-title="Municipality Change in Population Estimated from 2017 ACS Data to 2020 Census Count" 
switch="false" 
source="2020 Decennial Census Demographic and Housing Profile Data; LRTP 2045 Demographic Table Data"> 
</rpc-table> 


What is important to note is that (1) White student populations may have had an easier time living off-campus to complete their studies during COVID and (2) that the change to the Census questions will mean all areas of the country will have increases to the Some Other Race and Two or More Races categories. Together, these differences would also mean that the proportions of race populations are also not accurate to the post-COVID differences.

The numbers reported by the decennial census will have impacts on future grant work and for CUUATS in assessing and addressing underserved populations. This may also have other impacts on how transportation agencies in the region make planning decisions. Ultimately, because CUUATS suspects the count produced misleading population numbers, the LRTP will use population projections based on 2019 ACS data with modest population to be due to the lack of in-person classes at the University of Illinois campus while the Census count was occurring. 