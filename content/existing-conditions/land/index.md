---
title: "Land Use"
date: 2023-5-10
draft: false
Weight: 20
bannerHeading: Land Use
bannerText: >
  Land use and planning principles are critical to understanding the impact of
  future transportation investments.
---

The distribution and concentration of people and land uses within the urban
area can increase or decrease transportation options for people and businesses
accessing goods, services, employment, education, open space, and other
resources. Effective coordination of land use development and transportation
planning can produce policies and strategies that preserve and enhance valued
natural and cultural resources and facilitate healthy, sustainable communities
and neighborhoods.

This section provides an overview of local land use information including
population density, land use distribution and acreage, and the locations of
archaeological and historical sites. Municipal and residential population density
are both documented in the following text to help illustrate how the
municipalities in the metropolitan planning area have developed since 1990.
Population density alone does not provide a complete picture of how different
municipalities grow and plan for future development. For a more in-depth picture
of local growth practices and policies, see planning documents specific to each
municipality.


<br>


## Population Density ##

### Municipal Population Density ###


### Residential Population Density ###


<br>


## Land Use Distribution ##

Since 2017, the proportion of different land uses in the metropolitan planning area has not changed significantly. Land area designated for residential uses has remained stable. Agriculture land has seen a slight increase in land area and Institutional and Commercial lands experienced decreases. Because some land was reclassified between 2017 and 2022, parcels that the University of Illinois Urbana-Champaign utilizes for agricultural research were deemed as institutional in 2017 and agricultural in 2022. Thus, the land changes amongst categories could be attributed to changes in classification. 

<iframe src="https://maps.ccrpc.org/lrtp2050-land-use/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying current land-use in the metropolitan planning area by
  parcel-level tax data.
</iframe>

<rpc-table
  url="Land Use_Percentage_2017-2022.csv"
  text-alignment="1,r"
  table-title="Land Use in the MPA, 2017 and 2022"
  switch="false"
  source=" Champaign County, Tax Assessor, 2017 and 2022 Parcel-level Data">
</rpc-table>


A portion of parcels changed land use designation from 2017 to 2022. The following chart displays the change in square miles of those parcels.


<rpc-chart 
url="area_change_of_changed_parcels.csv" 
type="horizontalBar" 
colors="lime" 
columns="1,2" 
stacked="false" 
x-label="Square Miles"
switch="false" 
legend-position="bottom" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
source= "CUUATS, 2023" 
chart-title="Difference in Area for Parcels with Changed Land Use, 2017-2022" 
source-url="https://ccrpc.gitlab.io/sidewalk-explorer/"></rpc-chart> 


The following map displays the 2022 land use of the parcels that have changed land use designation within the Metropolitan Planning Area (MPA) from 2017 to 2022.


<iframe src="https://maps.ccrpc.org/lrtp2050-changed-land-use/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying change in land use for parcels in the MPA from 2017 to 2022.
</iframe>



<br>


## Archaeological and Historical Sites ##

Archaeological and historical resources in the metropolitan planning area include buildings, sites, districts, structures, and objects. The map below displays locations and types of archaeological and historical resources, the majority of which are found within or near the University of Illinois campus, Downtown Champaign, and Downtown Urbana.  

Any new or proposed transportation development needs to take measures to avoid adverse impacts such as damage, destruction, or removal of these features. Furthermore, transportation-related development should seek to preserve the larger setting of these structures when the setting is a significant element of the property, such as in the case of archeological areas. Most of the archeological areas encompass the waterways that are located within the metropolitan planning area like Boneyard Creek. There are also a number of Historic Bridges in the area, including near the Boneyard Creek Second Street Basin in Champaign and the bridge connecting S East St to the Mahomet Village Bike Trail in Mahomet.  

<iframe src="https://maps.ccrpc.org/historic-sites-and-archaeological-areas-in-the-mpa-2021/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying historic sites, buildings, and other archaeological places of interest within the metropolitan planning area.
</iframe>
