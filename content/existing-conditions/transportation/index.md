---
title: "Transportation"
date: 5-3-2023
draft: false
Weight: 50
bannerHeading: Transportation
bannerText: >
  Understanding how the transportation system currently works with regard to
  the quantity of the infrastructure and frequency of use, planners can better
  assess how the system should evolve into the future.
---

The following sections provide an overview of transportation behavior in the
community including each mode in the local transportation system. Data regarding
infrastructure, safety, and ridership are included where available.

## Transportation System Network

The Champaign-Urbana MPA is at the crossroads of three major interstates. The MPA is served by I-57, connecting the region with Chicago to the north and Memphis to the south; I-74, connecting the region with Indianapolis to the east, and Bloomington and Peoria to the northwest; and I-72, connecting the region with Decatur and Springfield to the west. 

 <iframe src="https://maps.ccrpc.org/lrtp2050-transportation-network-by-speed-limit/" 
    width="100%" height="600" allowfullscreen="true"> 
    Map of the transportation network in the metropolitan planning area by speed limit. 
    </iframe> 
 

## Commuting Behavior 

According to 2021 American Community Survey (ACS) commuting data for workers 16 years and over, the majority of MPA residents choose to drive alone to work rather than walking, biking, or using transit. However, these rates differ significantly depending on the municipality. In the cities of Urbana and Champaign, commuters choose to walk, bike, and take transit to work at significantly higher rates than the other local municipalities and national averages. For instance, commuters in the City of Urbana utilize public transportation to travel to work over twice as much as the United States overall.   

Only approximately 52 percent of Urbana commuters and 64 percent of Champaign commuters choose to drive alone to work compared with 83 percent of commuters in Mahomet and 73 percent of commuters nation-wide. The wider range of employment opportunities located in the Cities of Champaign and Urbana and the lack of mode choices to travel to and from the surrounding villages contribute to the heavy reliance on personal motor vehicles in these areas.  

<rpc-chart 
url="acs-commutework-2021.csv" 
type="bar" 
colors="orange,red,green,indigo,lime,gray,blue" 
stacked="true" 
switch="false" 
columns="1,2,3,4,5,6,7,8" 
y-label="Percent of Workers Over 16 Years" 
y-max="100" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Commuting Mode for Workers 16 Years and Older, 2021" 
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table S0801, 18 April 2023" 
source-url="https://data.census.gov/table?g=160XX00US1707211,1712385,1746136,1767860,1775601,1777005&tid=ACSST5Y2021.S0801"> 
</rpc-chart> 

The average commute time in MPA municipalities ranges from 15.2 minutes to 19.9 minutes. Over 90 percent of Champaign and Urbana workers commute less than half an hour to work, nearly half of the trips fall under 15 minutes. The longest commute times are in Mahomet followed by Bondville, where commute time exceeds 30 minutes for 14.0 and 13.5 percent of workers, respectively.  

For most workers in the MPA, average commute times have decreased since 2017. Savoy experienced the largest decrease in commute time, dropping over 4 minutes, followed by Mahomet where worker commutes decreased by 1 minute. Bondville is the only municipality that experienced an increase in average travel time, increasing just over 1 minute. 

<rpc-table text-alignment="1,r" 
url="acs-travel-time-2021.csv" 
table-title="Commuting Travel Time for Workers 16 Years and Older, 2021" 
switch="false" 
rows="1,2,3,4,5,6,7" 
source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table S0801, 18 April 2023" 
source-url="https://data.census.gov/table?g=160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSST5Y2021.S0801"> 
</rpc-table> 


<rpc-chart
url="acs-travel-time-2017-2021.csv"
type="bar"
colors="indigo,blue" 
stacked="false"
legend-position="top"
legend="true"
y-label="Average Commute Time (in Minutes)"
tooltip-intersect="true"
chart-title="Commuting Travel Time for Workers 16 Years and Older, 2017 vs 2021"
source-url="https://data.census.gov/table?g=160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSST5Y2021.S0801"
source="U.S. Census Bureau, 2013-2017 and 2017-2021 American Community Survey 5-Year Estimates, Table S0801, 18 April 2023">
</rpc-chart> 



The number of households working from home has sigificantly increased with the onset of the COVID-19 pandemic. In 2019, the national average was roughly 5 percent and increased to nearly 10 percent by 2021. This increase is similarly reflected in most municipalities within the MPA. The chart below displays the percentage of households that reported working from home in 2019 and 2021.


<rpc-chart
url="work_from_home_2019-2021.csv"
type="bar"
colors="orange,red"
stacked="false"
legend-position="top"
legend="true"
y-label="Percentage of Households"
tooltip-intersect="true"
chart-title="Percentage of Households Working from Home, 2019-2021"
source-url="https://data.census.gov/table?q=s0801&tid=ACSST1Y2021.S0801"
source=" U.S. Census Bureau, 2015-2019 and 2017-2021 American Community Survey 5-Year Estimates, Table S0801">
</rpc-chart> 


<br> 


## Complete Streets 

In 2023, the Champaign Urbana Urbanized Area Transportation Study (CUUATS) approved and adopted an updated [CUUATS Complete Streets Policy, replacing the 2012 resolution](https://ccrpc.gitlab.io/complete-streets-policy/). This policy promotes “Complete Streets” principles for all transportation infrastructure projects carried out within the Champaign-Urbana Urbanized Area, whether by the Illinois Department of Transportation, Champaign County, the Champaign-Urbana Mass Transit District, the Cities of Urbana and Champaign, the Village of Savoy, or the University of Illinois. The principles of this Complete Streets Policy are to design, build, maintain, and reconstruct public streets for pedestrians, cyclists, motorists, transit riders, emergency responders, and all people of diverse socio-demographics and varying abilities to ensure transportation is safe, convenient, and equitable for all people in the community. 
 

<br> 


## Pedestrians and Persons with Disabilities 

The ACS estimates 16 percent of Urbana workers and 10 percent of Champaign workers walked to work in 2021, compared with just 2.5 percent in the U.S. overall. Although ACS commuting data is the only reliable dataset available for transportation mode choice in our region, it does not likely paint a full
picture of walking behavior, as many choose to walk recreationally. 

Walking has many advantages for those without physical or environmental
limitations; it is healthy, sustainable, and free. Some people may choose to
walk only during exceptional weather days, or when a short travel distance
reduces the efficiency of other modes. For others, walking may play a more
extensive role in their everyday lives. Regardless of the extent, nearly
everyone relies on the pedestrian network to some degree.

### Pedestrian Safety ###

Pedestrians and people with disabilities are some of the most vulnerable people using our transportation network due to lack of protective gear and slow travel speed compared to buses, cars, and even bicycles. Thus, pedestrians can find themselves in crashes with the motorists that share the transportation network with them. Like all crashes, these vary in severity: A-injuries involve one or more person in the crash receiving an incapacitating injury; B-injuries involve one or more people in the crash receiving apparent, moderately severe injuries that were not disabling in nature.

Of the total 219 pedestrian crashes from 2018 to 2022, about 4 percent were fatal crashes. Of the total motor vehicle crashes involving pedestrians, nearly 30 percent resulted in a fatality or incapacitating injury (A-injuries). Fatal pedestrian crashes account for about 23 percent of all traffic fatalities and 14 percent of A-injuries (an increase from 9 percent from 2012-2016). Overall, local pedestrian crashes make up less than 2 percent of overall crashes. It should be noted that "No Injuries" does not account for near misses. 
 
 
<rpc-table 
url="Pedestrian_crashes_by_injury_type (2018-2022).csv" 
text-alignment="1,r" 
table-title="Pedestrian Crashes in the MPA, 2018-2022" 
switch="false" 
source=" Illinois Department of Transportation, 2018-2022 Crash-level Data" 
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"> 
</rpc-table> 

<rpc-chart
url="Pedestrian_crashes_by_age (2018-2022)_for_web.csv"
type="bar"
colors="indigo,blue"
stacked="false"
switch="false"
y-label="Number of Pedestrians in Crashes"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2018-2022 Person-level Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Number of Pedestrians involved in Crashes by Age in the MPA, 2018-2022"></rpc-chart>


The map below shows pedestrian crashes in the MPA from 2018 to 2022. The majority of pedestrian crashes occurred within the city limits of Champaign and Urbana. There were a total of nine fatal pedestrian crashes during these years, with five occurring in Champaign and four in Urbana. Of the fatal pedestrian crashes in Champaign, two occurred along Neil St at the intersections of Bradley Avenue and Neil Street and Windsor Road and Neil Street. Other fatalities occurred at W University Avenue near Eisner Park, S Duncan Road near W Kirby Avenue, and W Kirby Avenue near Hessel Park. In Urbana, two fatal pedestrian crashes were located along N Vine Street at E Water Street and E Main Street. Other fatalities occurred at E Main Street just south of E University Avenue and N Cunningham Avenue, just north of the University Avenue intersection.

 <iframe src="https://maps.ccrpc.org/lrtp2050-ped-crashes-2018-2022/" 
    width="100%" height="600" allowfullscreen="true"> 
    Map of the pedestrian crashes within the metropolitan planning area from 2018 to 2022. 
    </iframe> 


 #### Sidewalk Network ####

The accessibility of the pedestrian network is vital to local mobility. Families with young children, the elderly, and people with disabilities become more independent when pedestrian infrastructure is more accessible. An accessible pedestrian network enables all people to experience the surrounding community while decreasing the risk of injury for all transportation network users. 

The [Sidewalk Network Inventory and Assessment](https://ccrpc.gitlab.io/sidewalk-explorer/) for the urbanized area, carried out by the Champaign County Regional Planning Commission, began with baseline data collection in 2014. This data has been updated every summer to monitor improvements and progress towards increasing compliance with Americans with Disabilities Act (ADA) standards. The main components of sidewalk accessibility include various characteristics of sidewalks, curb ramps, crosswalks, and pedestrian signals. Current overall compliance and condition scores for each of the facility types assessed are provided in the tables and charts below. Compliance scores determine the extent to which a feature meets standards set by the ADA, while the condition scores capture relevant qualitative features not covered in the compliance evaluation. 

Crosswalks in the metropolitan planning area had the best compliance scores with nearly 95 percent of crosswalks scoring above 90 out of 100. Pedestrian signals and sidewalks received the lowest compliance scores, both with about 11 percent receiving scores above 90. Nearly half of pedestrian signals received the lowest range of compliance scores. While curb ramps also need improvement, they performed better than the pedestrian signals, with about 48 percent of compliance scores above 90. The biggest improvement from the 2018 compliance scores are the sidewalks. Currently, nearly 9 percent of sidewalks fall within the 60 or less range, compared to nearly 28 percent of sidewalks in 2018.

 
<rpc-table 
url="Sidewalk-compliance-scores-2022.csv" 
text-alignment="1,r" 
table-title="Sidewalk Network: Compliance Scores in the MPA, 2022" 
switch="false" 
source=" CUUATS, Sidewalk Explorer, 18 April 2023" 
source-url="https://ccrpc.gitlab.io/sidewalk-explorer/"></rpc-table> 
 

<rpc-chart 
url="sidewalk-compliance-score-percent-2022.csv" 
type="horizontalBar" 
colors="lime,blue,indigo,orange,red" 
x-max="100" 
x-min="0" 
switch="false" 
stacked="true" 
legend-position="bottom" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
source=" CUUATS, Sidewalk Explorer, 18 April 2023" 
source-url="https://ccrpc.gitlab.io/sidewalk-explorer/" 
chart-title="Sidewalk Network: Compliance Scores in the MPA (Percent), 2022"></rpc-chart> 

  
The chart below illustrates the percent change in compliance scores for each of the sidewalk network features between 2018 and 2022. The greatest positive change occurred with sidewalks. There was nearly a 12 percent increase in sidewalks with compliance scores between 80 and 90 out of 100. Additionally, sidewalks in the lowest compliance score ranges decreased by over 19 percent, indicating that sidewalk scores are increasing. Progress also occurred in many curb ramp features. Curb ramps decreased in all lower compliance score ranges and scores in the highest range (above 90) increased by about 4 percent.  
 

Condition scoring is also completed for sidewalks and curb ramps. These scores account for conditions such as deteriorating surface conditions, grass over-growth, faults, and cracks. Nearly 40 percent of sidewalk mileage and 67 percent of curb ramps received condition scores above 90.


<rpc-table 
url="sidewalk-condition-scores-2022.csv" 
text-alignment="1,r" 
table-title="Sidewalk Network: Condition Scores in the MPA, 2018 vs 2022" 
switch="false" 
source=" CUUATS, Sidewalk Explorer, 9 May 2023" 
source-url="https://ccrpc.gitlab.io/sidewalk-explorer/"></rpc-table> 


<br> 


## Bicycles 

The ACS estimates 5 percent of Urbana workers and nearly 2 percent of Champaign workers rode a bicycle to work in 2021 compared to 0.5 percent in the U.S. overall. As with other commuting data, these percentages do not include children under 16 years of age, or bicycle trips associated with recreation, exercise, and personal errands. Based on results from the 2012 National Survey of Pedestrian and Bicyclist Attitudes and Behaviors, approximately 78 percent of bicycle trips related to recreation, exercise, or personal errands. 

The map below illustrates the location of various types of bicycle facilities in the MPA. Champaign and Urbana host the majority of the facilities, but Mahomet and Savoy have also established a smaller but growing network of bicycle facilities. Within the municipality boundaries of Champaign, Urbana, and Savoy, nearly 73 percent of bicycle facilities are shared-use paths, followed by bike lanes at about 15 percent. Mahomet has 27 bicycle facilities, which are all shared-use paths. Tolono has one shared-use path. There are currently no designated facilities for bicycles in Bondville. 

 
<iframe src="https://maps.ccrpc.org/lrtp2050-bike-facilitie-category-2022/" 
  width="100%" height="600" allowfullscreen="true"> 
  Map of the bike and trail facilities within the metropolitan planning area. 
</iframe> 


Since the LRTP 2045, the MPA has experienced changes to the bicycle infrastructure. The following map shows the trails that were constructed from 2018 to 2022: 


<iframe src="https://maps.ccrpc.org/bicycle-facilities-since-lrtp-2045/" 
  width="100%" height="600" allowfullscreen="true"> 
  Map of the bike and trail facilities added to the network from 2018-2022 within the metropolitan planning area. 
</iframe> 


The mileage of bicycle facilities illustrates the community's continued support for shared-use paths and bicycle lanes in the local bicycle network. Of the total bicycle facility mileage in the MPA, an estimated 65 percent are shared-use paths and 22 percent are bike lanes. The other facility types, bicycle paths, sharrows, shared bicycle/parking lanes, and bicycle routes, collectively comprise about 13 percent of the network mileage.

<rpc-chart
url="bicycle_facilities_miles_2018vs2021.csv"
type="bar"
colors="lime,blue"
stacked="false"
legend-position="top"
legend="true"
y-label="Miles"
tooltip-intersect="true"
chart-title="Total Mileage by Facility Type, 2018 vs 2021">
</rpc-chart> 


#### Bicycle Safety 

Like pedestrians, bicyclists are some of the most vulnerable users of our transportation network, travelling in close proximity to much heavier and faster motorized vehicles. Of the total 203 bicycle crashes that occured in the MPA from 2017 to 2021, only 4 percent of them resulted in no injuries. Due to this vulnerability, prioritizing bicycle safety is fundamental to encouraging and supporting bicycling in our community. 

The map below shows bicycle crashes in the MPA from 2018 to 2022. The majority of bicycle crashes occurred within the city limits of Champaign and Urbana. Two fatal crashes occurred in 2020. The first was in April 2020 in Mahomet. The second occurred just outside of Mahomet along W Oak Street/County Road 2200 N in October 2020.

 
<iframe src="https://maps.ccrpc.org/lrtp2050-bicycle-crashes-2018-2022/" 
  width="100%" height="600" allowfullscreen="true"> 
  Map of the bicycle crashes within the metropolitan planning area from 2018 to 2022. 
</iframe> 
 

Over 55 percent of crashes involving a bicycle in the MPA between 2018 and 2022 resulted in a “B-injury”, meaning one or more people involved in the crash received apparent, moderately severe injuries that were not disabling in nature. Eighteen percent of crashes resulted in one or more “A-injuries”, meaning one or more people involved in the crash received an incapacitating injury. This is an increase from 15 percent from 2012-2016. One percent of all reported bicycle crashes were fatal. It should be noted that "No Injuries" does not account for near misses. 
 

<rpc-table 
url="Pedalcyclist_crashes_by_injury_type (2018-2022).csv" 
text-alignment="1,r" 
table-title="Bicycle Crashes in the MPA by Severity, 2018-2022" 
switch="false" 
source=" Illinois Department of Transportation, 2018-2022 Crash-level Data" 
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"> 
</rpc-table> 


The bar chart below graphs the age distribution of bicyclists involved in crashes from 2012 to 2016. The largest number of bicyclists involved in crashes were ages 15-19, followed closely by ages 20-24. Given the substantial presence of students in the community from the University of Illinois that would fall into these age ranges, these numbers likely represent students. Across all age groups, male bicyclists had significantly greater involvement in bicycle crashes than female bicyclists.


<rpc-chart
url="Pedalcyclist_crashes_by_age (2018-2022)_for_web.csv"
type="bar"
colors="indigo,blue"
stacked="false"
switch="false"
y-label="Number of Bicyclists in Crashes"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2018-2022 Person-level Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Bicyclists involved in Crashes in MPA by Age and Gender, 2018-2022">
</rpc-chart>


<br> 


## Transit

Public transportation is a sustainable and cost-effective option for traveling around the Champaign-Urbana community. Buses accommodate more people than personal vehicles with fewer resources. Fewer single-occupancy vehicles on the road lead to fewer emissions, less parking demand, less traffic congestion, and better population health. While public transportation itself is not considered active transportation, research shows that riders of public transportation are more physically active due to the multi-modal nature of using public transit, such as having to walk or bike in between stops. Studies show that transit riders get 8-33 additional minutes of physical activity each day compared with non-transit riders and show better physical health than those who regularly drive personal motor vehicles.  

The Champaign-Urbana Mass Transit District (MTD) provides a full range of mobility services to the urbanized area. The main fixed-route bus service includes nearly 2,000 bus stops and 70 different daytime, weekend, night, and late-night routes. MTD also provides ADA Paratransit service, C-CARTS rural service, Half Fare Cab, SafeRides, and MTD Connect to make sure people of all ages and abilities can safely get where they need to go. 
In addition to these services provided by MTD inside the MPA, MTD also operates under contract with Champaign County, the Champaign County Area Rural Transit System (C-CARTS). C-CARTS provides fixed route and demand response services to riders traveling to or from rural Champaign County.

In addition, MTD has contracts with the University of Illinois Urbana-Champaign and the Urbana School District to provide transportation services to university students, faculty and staff as well as Urbana public middle and high school students.

According to ACS 2021 data, a little over 5 and 9 percent of workers in Champaign and Urbana took the bus to work each day, with the national average at just over 4 percent. The rate of workers taking the bus to get to work in Savoy was just over the national average at 4.5 percent. Because the villages of Bondville, Mahomet, and Tolono are outside the MTD service area, the percent of workers using public transit to get to work was zero. 


#### Ridership

As expected, MTD ridership displayed a significant decline with the onset of the COVID-19 pandemic in 2020. From 2019 to 2020, ridership dropped 56 percent from 11.2 million to 4.9 million annual riders. Multiple factors contribute to this decline, including but not limited to, COVID-19 travel restrictions, University classes being held online, the rise of work from home, and anxiety regarding public health.  

Prior to this decline in ridership, MTD ridership hovered around 11 to 12 million, peaking at 13.6 million in 2014 and then 12.5 million in 2016. In 2016, MTD introduced a new bus route specifically for University students to utilize to get around campus called the 21 Raven. The spike in ridership that year is most likely attributed to the increase in student trips. In 2017, construction began on Multimodal Corridor Enhancement Projects (MCORE) along five major corridors in the University District. The MCORE project was completed in 2021. Because of the construction, existing bus routes on campus were rerouted, which could explain the dip in ridership during these years of construction.  

Since 2020, ridership has slowly increased as students return to campus and people begin to travel within the community more often. In 2021, ridership increased to about 5.5 million rides. By the end of 2022, MTD ridership was up to 7.7 million rides. Though it is still 35 percent lower than ridership in 2019, ridership appears to be on an upward trajectory. However, MTD service is not running at the same level as pre-pandemic. It is suspected that this is because of staffing challenges, increased health concerns and awareness, and the shift to remote work that has impacted the route service levels. 


<rpc-chart url="mtd_ridership_2007-2022.csv"
  chart-title="MTD Ridership, 2007-2022"
  x-label="Year"
  y-label="Number of Rides"
  type="line"></rpc-chart>

In 2023, the price for an annual bus pass is $60 (monthly is $20), a one-way ride with a free transfer is $1, and a All-Day Pass on the weekends is $2. This is more affordable in comparison to the $84 annual fee in 2019. 

Rides for veterans, senior citizens, persons with disabilities, and children under 46 inches in height are free. All eligible University of Illinois students, faculty, and staff have unlimited access to MTD service with their valid University-issued ID card. University students pay a mandatory fee each semester for this unlimited access, which was $71 for the 2022-2023 academic year. A 2019 MTD survey reported that 60% of respondents take the bus to reach their college or university and 27% of respondents use transit to reach their place of employment. This shows that a large portion of MTD riders are university students or affiliates. 

#### Projects 

Because of the multimodal nature of transit, MTD works with local agencies to support walking, biking, and ride share activities. Recent collaborations include Residents Accessing Mobility Providing Sidewalks (RAMPS), C-U Safe Routes to School (SRTS), Zipcar car share, VeoRide bike share, and Multimodal Corridor Enhancement Projects (MCORE).  

In 2015, MTD, with the support of the City of Champaign, City of Urbana, and the University of Illinois, applied for and received $15.7 million from a competitive federal grant program, Transportation Investment Generating Economic Recovery Grant (TIGER), for the construction of MCORE within the University District. Now complete, MCORE offers complete street corridors connecting the Cities of Champaign and Urbana to the University of Illinois, thus improving transit travel between the cities and the University, create new economic opportunities in the surrounding commercial areas, and improve local quality of life. The project includes a multimodal network of roads, on-street bike lanes, shared lane markings, bus-only lanes, and other transit services that enhance mobility for residents and visitors, particularly non-drivers, persons with disabilities, senior citizens, and economically disadvantaged populations. 

As the first transit agency in the United States with a hydrogen fleet of vehicles fueled solely by its own 100% renewable source, MTD has been a leader in adopting new technologies to reduce the environmental impact of the local transportation system. With its own hydrogen fuel production station, MTD fuels its electric buses with hydrogen, the buses use the fuel to produce energy, and the only output produced is water vapor. These vehicles consume 25 percent less fuel than their diesel-only vehicles, emit fewer greenhouse gases, and produce less noise when in operation. In 2022, MTD had a fleet of 118 buses- 4 diesel, 2 hydrogen fuel cell, and the remaining 95 percent of buses being hybrid diesel-electric. In 2018, 80 percent of MTD's bus fleet was hybrid diesel-electric, up from 9 percent in 2009. This displays a significant increase in hybrid diesel-electric buses over the past decade and MTD's commitment to lower emissions in the community.  



#### Champaign County Area Rural Transit System 

Community planning efforts to establish a Champaign County rural public transit system have been ongoing since the 1970s. In 1995, a preliminary needs assessment identified a significant need for rural public transportation and a more comprehensive study was completed in 2004, which confirmed the need for rural public transportation and suggested the need would continue to grow as the population ages. In 2008, Champaign County started a planning process which involved local stakeholders working together to identify rural transportation needs and resources and select a rural public transportation operator at the end of the process. In 2011, after completing this process, CRIS Rural Mass Transit District (CRIS-RMTD) was identified as the most suited operator for Champaign County.

CRIS Rural Mass Transit District began operating rural general public transportation in Champaign County in February 2011 Mondays through Fridays from 7:00 a.m. to 4:00 p.m. Initially, service was provided in the northeast quadrant (Village of Rantoul and surrounding areas) of the county where surveys demonstrated the highest need for rural transit service. In May 2013, CRIS expanded service to the entire rural Champaign County and service hours were extended to Monday through Friday from 6:00 a.m. to 6:00 p.m. 

In February 2014, CRIS ceased providing service to Champaign County. In order to continue to meet the need for rural public transit service in Champaign County, on October 1, 2014, the Champaign-Urbana Mass Transit District (MTD) began operating a new rural transit service under the name Champaign County Area Rural Transit System (C-CARTS). C-CARTS currently provides general public rural transportation in Champaign County, Monday through Friday from 6:00 a.m. to 6:00 p.m. In 2019, C-CARTS utilized thirteen, 14-passenger buses. In 2023, C-CARTS operates twelve buses. 


##### Eagle Express 

In November 2016, C-CARTS expanded rural transit services in Rantoul after signing a service contract with the Village. C-CARTS currently operates fixed-route and demand-response services in Rantoul. The [Eagle Express](https://c-carts.com/deviated-fixed-routes/), a fixed-route, stops at designated bus stops along three established routes within Rantoul, allowing passengers to board at any of the 65 bus stops in the village. Input from the public, employers, and employees helped create the fixed-route system. Residents who desire a custom trip, also known as a ‘demand-response’ trip, may call 48-hours in advance. Demand-response trips are not available during the times that C-CARTS operates the fixed-route system. Riders under the age of 12 may ride for a reduced rate at $1 one-way. 


##### Rantoul Connector 

The [Rantoul Connector](https://c-carts.com/deviated-fixed-routes/) links Rantoul to specific stops in Champaign-Urbana Urbanized area. Most riders utilize this route to go to medical appointments or work in the urbanized area. The service increases the economic capabilities and improves the health of Rantoul residents who would otherwise not be able to make trips to the urbanized area. The Rantoul Connector service operates approximately every 60 minutes from 5:00am to 8:00am and again from 3:00pm to 6:00pm. 


##### Ridership 

Both fixed-route and demand-response trips in Rantoul have increased since 2018. Fixed-route ridership increased from 758 rides in August 2018 to 1,949 rides in August 2020. Fixed-route ridership peaked in August 2021 at 2,891 rides. Demand-response trips increased slightly from 2018 to 2019, then decreased from 1,321 rides in August 2019 to 534 rides in August 2021.

<rpc-chart url="ccarts_six_month_ridership.csv"
  chart-title="C-CARTS Fixed-Route and Demand-Response Ridership, 2017-2022"
  x-label="Year"
  y-label="Number of Rides"
  type="line"
  point="true"
  point-radius=".5"></rpc-chart>

<rpc-chart url="ccarts_total_ridership_six_months.csv"
  chart-title="C-CARTS Total Ridership, 2017-2022"
  x-label="Year"
  y-label="Number of Rides"
  type="line"></rpc-chart>


Between March 2020 and August 2021, C-CARTS suspended fare enforcement for all services, causing ridership to increase by over 30% on the Eagle Express, despite being in the midst of the pandemic. While many transit agencies suspended fares during that time, [transit ridership nationally sank by 80 percent](https://www.apta.com/news-publications/press-releases/releases/public-transportation-ridership-rises-to-more-than-70-percent-of-pre-pandemic-levels/#:~:text=After%20falling%20to%2020%20percent,while%20delivering%20safe%2C%20reliable%20service.). Because factories within Rantoul continued normal operations throughout the COVID-19 pandemic, many employees relied on C-CARTS for transportation to work. 

Demand Response trips decreased in April 2020 and have remained lower than pre-pandemic numbers. This is likely due to challenges in hiring and retention. 

According to [C-CARTS data](https://c-carts.com/performance/), the majority of all rural transit trips are for the purpose of getting to work and visiting medical facilities. Other trip purposes include social, educational, shopping, and personal reasons. 


### Human Services Transportation
Human services transportation providers help meet the transportation needs of the elderly, individuals with disabilities, and persons and families with low income(s). Often, these individuals and families have four types of limitations that preclude them from driving: 

- **Physical:** old age, blindness, paralysis, developmental disabilities, acute illness, etc. 
- **Financial:** Unable to purchase or rent a personal vehicle 
- **Legal:** Too young, loss of driver’s license, or driver’s license not obtained 
- **Self-imposed:** Choose not to own or drive a vehicle 

In order to meet the transportation needs of these populations, there are numerous human services transportation providers in the region and many opportunities exist to improve how these services function. In terms of accessibility, there are issues concerning ease of use, under-served and unserved areas, and a lack of centralized information. In terms of availability, issues of scheduling and temporal limitations are present. The reliability of the system is also an issue because of the frequency of service, in addition to restrictions that result from program eligibility requirements and trip purposes. More detailed information can be found in the [Champaign-Urbana Urbanized Area Human Services Transportation Plan](https://ccrpc.gitlab.io/ua-hstp/) which was updated March 2023. 

### CUUATS HSTP Transportation Providers ###


<rpc-table 
  url="CUUATS_providers.csv" 
  switch="false" ></rpc-table>


<br> 


## Automobiles 

According to the 2021 ACS Data, anywhere from 52 to 93 percent of municipality residents drive alone to work each day, with another 4 to 10 percent of workers carpooling within the MPA. There are many other trips not included in these numbers- such as trips for running errands, shopping, visiting the doctor, taking children to school, etc. 

Automobiles are convenient for allowing people to travel freely and connect them to long-distance destinations within and outside the community. However, the expense of purchasing, maintaining, and operating an automobile is prohibitively expensive for many households, underscoring the potential for multimodal transportation choices (i.e. car-sharing, public transit, bicycling, walking) to reduce household transportation costs. Moreover, all people within the community experience the byproducts of widespread automobile use including safety risks, emissions, noise, infrastructure costs, and traffic congestion. 

 
### Automobile Ownership 

Annual vehicle registration numbers from the Illinois Secretary of State provide an estimate of the total number of motor vehicles on local roadways. As of April 2023, the total number of vehicles registered in Champaign County is nearly 159,000. In 2017, the Secretary of State began recording electric vehicle ownership by county. In December 2017, Champaign County had 88 registered electric vehicles. In December 2018, that number increased 72 percent to 152 registered EVs. By December 2022, this number reached 642 registered EVs. As of April 2023, this number is 760 registered electric vehicles in Champaign County.

<rpc-chart url="EV_Ownership_CC_Dec2017_Dec2022.csv"
  chart-title="Electric Vehicle Ownership in Champaign County, 2017-2022"
  x-label="Year (December)"
  y-label="Number of EVs"
  type="line"
  source-url="https://data.ccrpc.org/dataset/alternative-fuel-stations-2045" 
  source=" https://www.ilsos.gov/departments/vehicles/statistics/electric/home.html"></rpc-chart>

According to the [CUUATS Annual LRTP 2045 Report Card](https://data.ccrpc.org/pages/lrtp-2045-report-card), the total number of publicly available alternative fuel stations in the MPA have increased from 44 in 2015 to 66 in 2021. While electric car charging stations have increased, other alternative fueling options in the region, E85 and liquefied petroleum gas (LPG), have decreased slightly during the same timeframe.
 
<rpc-chart url="alternative-fuel-stations-2021.csv"
  chart-title="Alternative Fueling Stations in the MPA, 2015-2021"
  x-label="Year"
  y-label="Number of stations"
  type="line"
  source-url="https://data.ccrpc.org/dataset/alternative-fuel-stations-2045" 
  source=" CCRPC Alternative Fuel Stations"></rpc-chart>



### Vehicle Access 

Household access to motor vehicles is an ACS dataset that can help illustrate the relationship between income and mobility. Not all households have access to a vehicle for a variety of reasons, including but not limited to lack of money to purchase and maintain a vehicle, lack of a driver’s license, or lack of desire or physical ability to drive or own a vehicle. For households and individuals without access to a vehicle, mobility depends on other methods of transportation, such as walking, bicycling, and public transportation, to participate in the activities that structure daily life (i.e. work, school, social engagement, shopping, physical exercise). For households with quality access to other forms of transportation, the lack of vehicle ownership can be a significant cost savings.  

In 2021, the estimated percentages of households without access to a motor vehicle in Champaign and Urbana were 9 percent and 10 percent, respectively. These percentages are over twice as much as the national average of households without vehicle access. Urbana experienced an increase in percentage of households without vehicle access, increasing from 8.5 percent in 2019 to 9.3 percent in 2020 and 9.7 percent in 2021. Champaign has also experienced a slight increase in households without vehicle access, increasing from 8.6 percent in 2019 to 8.8 in 2021.  
 
 <rpc-table 
  url="access_motor_vehicles_2021.csv" 
  text-alignment="1,r" 
  table-title="Access to Motor Vehicles per Household, 2021" 
  switch="false" 
  source-url="https://data.census.gov/table?q=B08201:+HOUSEHOLD+SIZE+BY+VEHICLES+AVAILABLE&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2021.B08201" 
  source=" U.S. Census Bureau, 2017-2021 American Community Survey 5-Year Estimates, Table B08201"></rpc-table> 

The increase in no vehicle access can be attributed to multiple factors. According to the [US Bureau of Transportation Statistics](https://www.bts.gov/content/average-cost-owning-and-operating-automobilea-assuming-15000-vehicle-miles-year), the average cost of owning and operating an automobile gradually increased over time, peaking in 2013 before beginning to decrease from 2013 to 2017. In 2018, the cost began to rise again and has continued, quickly surpassing the 2013 values. In 2019, the average total cost of owning and operating an automobile per 15,000 miles was $9,282. By 2022, this number increased to $10,729 per 15,000 miles. Thus, the cost of owning and operating a personal motor vehicle may prove to be too costly for some households.  

Perhaps the latest explanation for this increase is the shift to remote work due to COVID-19. From 2019 to 2021, the percentages of people working from home significantly grew in the MPA. In Champaign, the percentage increased from 3.6 percent in 2019 to 9 percent in 2021. In Urbana, this number more than doubled from 4 percent in 2019 to 9.7 percent in 2021. Savoy and Mahomet also experienced a significant increases, from 5 percent to 13 percent and 6 percent to just over 12 percent, respectively. The fact that more households in the MPA are working from home than ever before could have an impact on the number of people who have access to a vehicle, as households evaluate whether it is necessary to own a vehicle. 

Other factors that explain the increase in households without access to motor vehicles are the University efforts to minimize student vehicles in the campus district and improvements in walking, biking, and transit infrastructure that reduce reliance on vehicles. In 2018, VeoRide began its dockless bike share program in the MPA, which offers residents an alternative mode of travel outside of having a personal vehicle. 

For those communities in which vehicle access has increased (showing a decrease in no motor vehicle access), many households rely on personal automobiles as transport to work. Additionally, Tolono, Bondville, and Mahomet are not served by MTD, with many rely on motor vehicles as primary transportation. 

  <rpc-chart
  url="no_vehicle_access_2017-2021.csv"
  type="bar"
  colors="#c4c4c4,lime,blue"
  stacked="false"
  legend-position="top"
  legend="true"
  y-label="Percentage of Households"
  tooltip-intersect="true"
  chart-title="Households with No Motor Vehicle Access, 2017-2021"
  source-url="https://data.census.gov/table?q=B08201:+HOUSEHOLD+SIZE+BY+VEHICLES+AVAILABLE&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2021.B08201"
  source=" U.S. Census Bureau, 2013-2017, 2015-2019, and 2017-2021 American Community Survey 5-Year Estimates, Table B08201">
  <rpc-dataset
  type="line"
  fill="false"
  border-color="#24ABE2"
  border-width="1"
  point-radius="0"
  order="4"
  label="National Rate of No Vehicle Access in 2021"
  data="8.4,8.4,8.4,8.4,8.4,8.4">
  </rpc-dataset>
  </rpc-chart>


### Roadway Network Characteristics 


#### Functional Classification 
Functional classification is a process of categorizing roads primarily based on
mobility and accessibility. Mobility relates to operating speed, level of
service, and riding comfort. It is a measure of the expedited traffic conditions
between origin and destination. Accessibility identifies travel conditions with
increased exit and entry locations. IDOT uses the following classifications for
roadways throughout the state:

- **Major arterials** have the highest level of mobility and the lowest level of
  accessibility. Interstates and other major arterials serve trips through
  urban areas and long-distance trips between urban areas.

- **Minor arterials** serve shorter trips between traffic generators within urban
  areas; they have more access but a lower level of mobility than interstates
  and other principal arterials.

- **Major and Minor Collectors** provide both mobility and access by gathering trips from
  localized areas and feeding them onto the arterial network.

- **Local streets** are lower-volume roadways that provide direct land access but
  are not designed to serve through-traffic.


<iframe src="https://maps.ccrpc.org/lrtp2050-road-classification-2021/"
  width="100%" height="600" allowfullscreen="true">
  Map of the 2021 IDOT functional classification within the metropolitan planning area.
</iframe>



#### Annual Average Daily Traffic 

IDOT counts and [publishes annual average daily traffic (AADT) volumes](http://apps.dot.illinois.gov/gist2/) for
interstates and selected major arterials and collectors. The AADT map for the
metropolitan planning area uses the IDOT data to display the most frequently
traveled roadways in the area. Interstates I-74 and I-57 have the highest AADT
volumes. After interstate traffic, the highest volumes were recorded along
portions of the following roadways: Prospect Avenue, Mattis Avenue, University
Avenue/US-45, Neil Street/US-45, Lincoln Avenue, and Cunningham Avenue/US-45. 

<iframe src="https://maps.ccrpc.org/lrtp2050-traffic-counts-2021/"
  width="100%" height="600" allowfullscreen="true">
  Map of the 2021 IDOT average daily traffic counts within the metropolitan planning area.
</iframe>

Traffic counts vary at railroad crossings across the MPA. The highest traffic counts at railroad crossings include 20,001 to 50,000 annual average daily traffic counts (AADT). These are seen at the crossing on N Prospect Avenue, north of Bradley Avenue and south of I-74. Other high traffic crossings (10,001 to 20,000 ADT) include:

  -	N Neil Street (north of E Washington Street)
  -	N Market Street (north of E Washington Street)
  -	E Bradley Avenue (west of N Oak Street)
  -	N Lincoln Avenue and W University Avenue
  -	N Mattis Avenue (north and south of W Bradley Avenue)
  -	W Springfield Avenue (near S Duncan Road)


<iframe src="https://maps.ccrpc.org/lrtp2050-railroad-crossings-adt-in-mpa-2021/"
  width="100%" height="600" allowfullscreen="true">
  Map of the 2021 IDOT average daily traffic counts at railroad crossings within the metropolitan planning area.
</iframe>


### Automobile Safety 

#### Crash Rates per Vehicle Miles Traveled 
One measure of automobile safety performance is the number of crashes per the
total vehicle miles traveled (VMT). By using VMT, the resulting crash rate
accounts for the variance of motor vehicle usage between different regions. The
table and chart below use Illinois Department of Transportation data to compare
the MPA crash rate to the Illinois crash rate. 

**The following chart does not depict 2022 IL Crash Rates. It will be updated when the data is received.**

<rpc-chart
  url="Crash_per_VMT_2018-2022.csv"
  type="bar"
  columns="1,2"
  switch="false"
  legend-position="top"
  colors="indigo"
  y-label="Number of Crashes per 100M VMT"
  y-min="180"
  legend="true"
  grid-lines="true"
  tooltip-intersect="true"
  source=" Illinois Department of Transportation, 2017-2022 Crash-level Data"
  source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
  chart-title="Crashes per 100 Million VMT in the MPA and State, 2017-2022"><rpc-dataset
  type="line"
  label="IL Crash Rate"
  data="295.33,290.86,262.51,289.19,0"
  fill="true"
  order="7"
  border-color="blue"
  point-radius="4"></rpc-dataset>
</rpc-chart>

#### Fatal Traffic Crashes  
The following map shows the total 29 fatal automobile crash locations in the MPA from 2018 to 2022. Crashes were most frequently located within municipal boundaries. The City of Champaign had 12 total fatal crashes, the most fatal crashes of the municipalities in the MPA. Most of them occurred in the northern part of the city. The City of Urbana had a total of four fatal crashes, located on the western side of the city. Two fatal crashes occurred in Mahomet and one in Bondville. The MPA’s fatal crashes outside of municipal boundaries occurred between Mahomet and northwest Champaign, Bondville and Champaign, and south of Savoy. 

The map below shows the locations of fatal automobile crashes (as well as those resulting in A-Injury, B-Injury, C-Injury, and No Injuries) in the MPA between 2018-2022.

<iframe src="https://maps.ccrpc.org/lrtp2050-auto-crashes-2018-2022/"
  width="100%" height="600" allowfullscreen="true">
  Map of the fatal crash locations within the metropolitan planning area from 2018 to 2022.
</iframe>



<br> 


##  Freight 
A large part of the local economy relies on the freight transportation system. In 2019, CCRPC published the [Champaign-Urbana Region Freight Plan](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/) which provides a detailed look at the freight system in our region. 


### Truck Routes 
Prior to January 2020, roadways were divided into eight different classifications, designated as either state or local. Today, there are three classes of truck routes in the MPA: Class One (interstates, expressways, tollways); Class Two (State and Local Designated Highways with at least 11 ft lanes); Non-Designated (all other State and Local Highways). The following is a list of [designated truck routes](https://www.gettingaroundillinois.com/MapViewer/?config=DTRconfig.json) within the MPA:

-   **Class One**: Interstates I-57, I-72, and I-74

-   **Class Two**: Segments of U.S. 150, Bloomington Road, Church Street, Mattis Avenue, Prospect Avenue, Springfield Avenue, US 45/Neil Street/Dunlap Avenue, University Avenue, High Cross Road, Cunningham Avenue, Guardian Drive/Butzow Drive*, and IL 47 in Mahomet


*Truck routes locally designated by the City of Urbana

<iframe src="https://maps.ccrpc.org/lrtp2050-truck-routes/"
  width="100%" height="600" allowfullscreen="true">
  Map of the fatal crash locations within the metropolitan planning area from 2017 to 2021.
</iframe>



### Employment 
The percentage of workers in Champaign County working in the "Transportation and Warehousing" industry has increased over the last five years, from 3.6 percent in 2017 to 4.7 percent in 2021. In 2020, the number peaked at 4.9 percent. This increase can be attributed to the height of the COVID-19 pandemic when in-person shopping was limited and more households utilized online delivery services. Additionally, in 2019, Amazon built a warehouse in North Champaign at 1201 Newton Drive, contributing to the local industry's workforce. As of 2021, there were 149 establishments in Champaign County within the "Transportation and Warehousing" industry.  

 
<rpc-chart 
url="workers_in_transportation_2017-2021.csv" 
type="line" 
switch="false" 
stacked="false" 
y-label="Percentage of workers in industry" 
legend="false" 
grid-lines="true" 
tooltip-intersect="true" 
source="U.S. Census Bureau, ACS 5-Year Estimates (2017-2021)" 
source-url="https://data.census.gov/table?t=Industry&g=050XX00US17019&tid=ACSST5Y2021.S2404" 
chart-title="Percentage of Champaign County Workers in the Transportation and Warehousing Industry, 2017-2021"></rpc-chart> 
 

### Freight Movement 
Given Champaign County's landscape and current infrastructure, freight is typically transported through the region by truck or train. In 2017, eighty-five percent of the total goods tonnage, as well as 94 percent of total goods value, were moved by trucks on the county’s roadway network. The remaining total tonnage and value were moved by trains. Since the share of freight value carried by truck was greater than the share of tonnage carried by trucks, it seems that trucks were used to carry the region’s relatively higher-value, lower-weight goods.  

Compared with outbound movements, a higher percentage of inbound goods were moved by trucks. Additionally, all of the internal freight activities were transported by truck. Outbound and inbound commodity flows were fairly equal, but those outbound displayed lower tonnage and higher value, indicating that the county is exporting high value items.   

 <rpc-table 
  url="commodity_moved_by_truck_2017.csv" 
  text-alignment="1,r" 
  table-title="Total Commodity Tonnage and Value Moved by Truck, 2017" 
  switch="false" 
  source-url="https://ccrpc.org/documents/champaign-urbana-region-freight-plan/" 
  source="Champaign-Urbana Region Freight Plan"></rpc-table>

According to the Champaign-Urbana Region Freight Plan, the region's freight-intensive sectors include agriculture, manufacturing, wholesale and retail, construction and utilities, and transportation and warehousing. Cereal Grains such as Corn and Soybean are the largest commodity by tonnage, accounting for nearly one-third of truck imports and exports in the region in 2017. In the same year, Champaign County’s top trading partners by tonnage were Iowa, McLean County, Indiana, Cook County (Chicago), and Missouri.  


### Existing Infrastructure and Conditions

#### Pavement and Bridges
The Freight Plan set forth multiple goals to improve the region's freight system, such as preserving existing infrastructure which includes ensuring that pavement, roadways, and bridges are kept in good condition to lessen the need for critical repairs. This is measured by the percent of roadway mileage rated "Poor and Failed" in the MPA. In 2017, approximately 19 percent of roadway mileage was considered "Poor and Failed" by its respective jurisdiction, this number increased to 21 percent in 2020.  

#### Truck Parking 
From 2017 to 2020, there were seven truck parking facilities in Champaign County, totaling less than 150 total truck parking spaces in the county. [The Freight Plan aimed to increase the number of truck parking facilities in Champaign County from seven to ten or expand the square footage of truck parking areas by 40 percent by 2025](https://ccrpc.org/documents/champaign-urbana-region-freight-plan-2/freight-plan-report-card/number-area-of-truck-parking-areas-in-champaign-county/). This has not been accomplished and remains an important topic in long range planning.  

#### Safety 
Improving safety and quality of life are [two goals set forth in the region's freight plan](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/freight-plan-report-card/total-number-of-heavy-vehicle-involved-serious-injuries/). One of the objectives from the plan is to reduce the number of severe heavy vehicle traffic injuries by 50 percent from 2016 to 2025. Instead, from 2016 to 2019, the [five year rolling average of severe injuries amongst heavy vehicle traffic](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/freight-plan-report-card/total-number-of-heavy-vehicle-involved-serious-injuries/) has increased about 24 percent. Additionally, ensuring the safety of all road users (including pedestrians, bicyclists, truck and automobile users) is another goal from the plan that aims to eliminate all heavy-vehicle involved fatalities by 2025. In both 2018 and 2019 there were no heavy vehicles involved in traffic fatalities, indicating that this objective is headed in a positive direction. Finally, the total number of collisions between trucks and pedestrians and bicyclists has increased since 2017, as there were three crashes in 2019, despite the goal aimed at eliminating collisions by 2025.  

For more information on the Freight Plan objectives and goals, visit the [Freight Plan Report Card](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/freight-plan-report-card/).




<br> 


##  Trains  ##
The four railroad lines in the metropolitan planning area are owned by two rail companies: Canadian National Railroad and Norfolk Southern Railroad. Canadian National owns the north-south line with the heaviest traffic as well as the east-west line from downtown Champaign through Bondville. Norfolk Southern owns and operates the east-west line connecting Mahomet to Champaign-Urbana and the east-west line running through Tolono. These two rail companies move a significant number of freight foods through the MPA.

<iframe src="https://maps.ccrpc.org/ltrp2050-railroad-ownership-in-the-mpa-2021/"
  width="100%" height="600" allowfullscreen="true">
  Map of the railroads and their respective ownership within the
  metropolitan planning area.
</iframe>

On the MPA’s north-south Canadian National line, Amtrak operates three passenger train routes that stop at the Illinois Terminal in Downtown Champaign: the Saluki, Illini, and City of New Orleans. Each of these three routes has one daily trip northbound and one daily trip southbound. The Saluki and the Illini provide service between Carbondale and Chicago, whereas the City of New Orleans provides service between New Orleans and Chicago.  

Amtrak ridership has increased since 2000, with the greatest increase between 2006 and 2007 corresponding to the addition of the Saluki route. Since then, the ridership generally increased until it peaked in 2013, before beginning a gradual decline in ridership. However, in 2019 the ridership numbers increased significantly, most likely due to the closure of Suburban Express which transported many students to the Chicago suburbs during the academic year. In 2020 and beyond, ridership numbers decreased due to the onset of the COVID-19 pandemic and travel restrictions. From 2019 to 2020, ridership decreased 37 percent. Ridership numbers continue to remain lower than pre-pandemic but are on the rise.

<rpc-chart 
url="Amtrak-ridership-lrtp-report-card-2000-2021.csv" 
type="line" 
switch="false" 
y-label="Number of Passengers" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
source=" Amtrak, State of Illinois Amtrak Fact Sheet, Fiscal Years 2000-2022" 
source-url="https://www.bts.dot.gov/browse-statistical-products-and-data/state-transportation-statistics/amtrak-ridership" 
chart-title="Amtrak Ridership at Illinois Terminal, 2000-2022"></rpc-chart> 


The decline in ridership from 2013 to 2017 could in part be attributed to the consistent delays experienced by riders. Both CN and NS both scored a 'F' on the [2017 Annual Amtrak Host Performance Report Card](https://media.amtrak.com/wp-content/uploads/2018/03/CY2017-Report-Card-%E2%80%93-FAQ-%E2%80%93-Route-Details.pdf), meaning these railroads were responsible for over 1,500 minutes in delays per 10,000 train miles. Additionally, 90 percent of local Amtrak trains were delayed in 2017 due to Canadian National freight trains receiving priority. The north-south Canadian National rail line supports both passenger and freight traffic, though freight traffic generally has priority over passenger traffic.  

These ratings have seen improvement since then. Canadian National scored an 'A' rating on the [2021 Annual Amtrak Host Performance Report Card](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiLhKSH-sL-AhVlmWoFHSXqB8UQFnoECAsQAQ&url=https%3A%2F%2Fmedia.amtrak.com%2Fwp-content%2Fuploads%2F2022%2F03%2FHost-Railroad-Report-Card-2021-Final-v2.pdf&usg=AOvVaw28aBzfyB_gmHABXHrT6bEy), reducing its delays to under 900 minutes per 10,000 train miles. Norfolk Southern scored a 'D-' rating, just barely reducing the delay time to between 1,350 and 1,500 minutes per 10,000 train miles. All three Amtrak routes that stop at the Illinois Terminal are serviced by the Canadian National railroad. Of the three routes, the City of New Orleans was the only one to meet the on-time standard set by Amtrak, with 83 percent of on-time passengers. In fact, it was the only route that passed the standard in all of Amtrak for 2021. The other two routes, Illini and Saluki, failed to meet the standard, with only 72 percent of on-time passengers in 2021.  


<br> 


## Airplanes ##

Two airports, Willard Airport and Frasca Field, are located in the MPA. Willard Airport is a public airport owned by the University of Illinois and located southwest of Savoy in between I-57 and U.S. 45. Frasca Field is a private airport open for public use located north of Interstate-74 in Urbana. 

Willard Airport provides non-stop service to two destinations: Chicago, Illinois and Dallas, Texas. While similar-sized airports in the region generally have three to four carriers, all Willard flights are currently operated by one carrier, American Airlines.  The charts below show enplanements and flights at Willard Airport between 2000 and 2021.  

As expected, enplanements and flights at Willard Airport displayed a significant decrease with the onset of the COVID-19 pandemic, as did other regional airports. With travel restrictions, many people were not allowed to travel for certain periods of 2020 or did not feel comfortable flying amid the health crisis. Enplanements at Willard Airport decreased about 67 percent from 2019 to 2020. While there has been an increase in enplanements since 2020, the numbers remain significantly lower than those pre-pandemic.  


<rpc-chart 
url="Enplanements-at-willard-airport-lrtp-report-card_2000-2021.csv" 
type="line" 
switch="false" 
colors="blue,#c4c4c4,#e2e2e2,#A2A1A6" 
stacked="false" 
y-label="Number of Enplanements" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
source=" Bureau of Transportation Statistics, T-100 Market Data, 2010-2021" 
source-url="https://www.transtats.bts.gov/Data_Elements.aspx?Data=1" 
chart-title="Annual Enplanements for Regional Airports, 2000-2022"></rpc-chart> 
 

From 2019 to 2020, annual flights at Willard Airport decreased by nearly 50 percent. In 2021, Willard Airport experienced a slight increase in flights before decreasing again in 2022, this time lower than the number of flights at the height of the pandemic. This is due to the fact that the airport ended its flights to Charlotte, North Carolina in October 2021 due to lack of passenger demand.  


<rpc-chart 
url="Flights_2002-2022.csv" 
type="line" 
switch="false" 
y-label="Number of Flights" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
source=" Bureau of Transportation Statistics, T-100 Segment Data, 2000-2022" 
chart-title="Annual Flights at Willard Airport, 2002-2022" 
source-url="https://www.transtats.bts.gov/Data_Elements.aspx?Data=1"> 
</rpc-chart> 