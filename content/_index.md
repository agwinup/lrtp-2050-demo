---
title: Home
date: 2018-09-18T13:50:42-06:00
draft: false
hide_edit_btn: true
bannerHeading: "LRTP 2045"
bannerText: >
  As a community transportation policy document, the LRTP provides a regional
  transportation vision for 2050 to guide future transportation
  investments.
---

<h1 class="desktop-heading">Welcome to the Champaign-Urbana Long Range Transportation Plan (LRTP) 2050!</h1>
<h1 class="mobile-heading">LRTP 2050</h1>

This page is currently in the process of being updated.

<hr style="border: none; width:80%;border-top: 1px solid white;">

<h2 class="desktop-heading">The LRTP 2050 survey is now closed!</h2>
<h2 class="mobile-heading">The survey is now closed!</h1>

Thank you for your feedback and participation! We will continue to keep you updated as the LRTP 2050 progresses.

Please refer to this website for project information and updates.


<hr style="border: none; width:80%;border-top: 1px solid white;">

<div class="row" style="width: 80%; margin: 20px auto;">
  <div class="col-md-3 d-none d-md-block">
    <img src="Map_QR.png" alt="Map QR Code" width="90%">
    <span class="text-center">Scan the QR code to give your feedback</span>
  </div>
  <div class="col-md-9">
    <h2>Use the online map to give feedback on your transportation experience</h2>
    <p>Tell us more about your travel in C-U on our online map!</p>
    <a target="new" href="https://ccrpc.gitlab.io/transportation-voices-2050/" class="btn-custom btn-block w-lg-50 mx-auto">Click here to access the map</a>
  </div>
</div>