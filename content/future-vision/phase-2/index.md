---
title: "Phase 2"
date: 2018-09-18T13:50:42-06:00
weight: 10
layout: "future_vision"
bannerHeading: Phase 2
bannerText: >
  Welcome to the Phase 2 of the C-U Long Range Transportation Plan 2050
---

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Survey Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  </head>
  <body>
    <h2 class="section-divide welcome">Welcome</h2>
    <h3 class="welcome-flavour-text"> Welcome to the Phase 2 of the C-U Long Range Transportation Plan 2050 </h3>
    <hr class="future-vision-hr">
    <section class="anchor-link-buttons">
      <div class="row">
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#phase-1" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="fas fa-info-circle icon-size mr-2 mb-3 mb-md-0"></i> What have we learned in phase 1? </a>
        </div>
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#general-comment" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="far fa-comment icon-size mr-2 mb-3 mb-md-0"></i> Leave a comment </a>
        </div>
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#event" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="far fa-calendar-check icon-size mr-2 mb-3 mb-md-0"></i> Join us at an upcoming event </a>
        </div>
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#survey" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="fa-solid fa-bullseye icon-size mr-2 mb-3 mb-md-0"></i> Survey </a>
        </div>
      </div>
    </section>
    <section class="welcome">
      <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec elit dignissim cursus. Sed ac bibendum justo. Integer malesuada vehicula risus, ac facilisis justo dignissim ut. Nam fermentum metus in tellus bibendum, nec elementum ipsum dignissim.Pellentesque abitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sagittis elit non dolor laoreet, et volutpat dolor vehicula. Proin auctor volutpat metus id venenatis. Vivamus eu apien in massa maximus fringilla. Vivamus sollicitudin urna quam, eget eleifend arcu consequat eget. Quisque sed nulla bibendum, vulputate sapien ut, vehicula felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec elit dignissim cursus. Sed ac bibendum justo. Integer malesuada vehicula risus, ac facilisis justo dignissim ut. Nam fermentum metus in tellus bibendum, nec elementum ipsum dignissim.Pellentesque abitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sagittis elit non dolor laoreet, et volutpat dolor vehicula. Proin auctor volutpat metus id venenatis. Vivamus eu apien in massa maximus fringilla. Vivamus sollicitudin urna quam, eget eleifend arcu consequat eget. Quisque sed nulla bibendum, vulputate sapien ut, vehicula felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec elit dignissim cursus. Sed ac bibendum justo. Integer malesuada vehicula risus, ac facilisis justo dignissim ut. Nam fermentum metus in tellus bibendum, nec elementum ipsum dignissim.Pellentesque abitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sagittis elit non dolor laoreet, et volutpat dolor vehicula. Proin auctor volutpat metus id venenatis. Vivamus eu apien in massa maximus fringilla. Vivamus sollicitudin urna quam, eget eleifend arcu consequat eget. Quisque sed nulla bibendum, vulputate sapien ut, vehicula felis. </p>
    </section>
    <section class="alert">
      <article class="exclaim">
        <p class="section-intro">The C-U Long Range Transportation Plan (LRTP) is a vision for the future of transportation in Champaign-Urbana.</p>
      </article>
    </section>
    <hr class="future-vision-hr">
    <section class="phase-1">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fas fa-info-circle icon-size text-danger mr-3" aria-hidden="true"></i>
        <h2 id="phase-1">What did we learn in Phase 1</h2>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec elit dignissim cursus. Sed ac bibendum justo. Integer malesuada vehicula risus, ac facilisis justo dignissim ut. Nam fermentum metus in tellus bibendum, nec elementum ipsum dignissim.Pellentesque abitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sagittis elit non dolor laoreet, et volutpat dolor vehicula. Proin auctor volutpat metus id venenatis. Vivamus eu apien in massa maximus fringilla. Vivamus sollicitudin urna quam, eget eleifend arcu consequat eget. Quisque sed nulla bibendum, vulputate sapien ut, vehicula felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at fringilla nunc. Donec eu elit nec elit dignissim cursus. Sed ac bibendum justo. Integer malesuada vehicula risus, ac facilisis justo dignissim ut. Nam fermentum metus in tellus bibendum, nec elementum ipsum dignissim.Pellentesque abitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sagittis elit non dolor laoreet, et volutpat dolor vehicula. Proin auctor volutpat metus id venenatis. Vivamus eu apien in massa maximus fringilla. Vivamus sollicitudin urna quam, eget eleifend arcu consequat eget. Quisque sed nulla bibendum, vulputate sapien ut, vehicula felis. </p>
      <div class="row">
        <div class="col-md-6 mb-3 mb-xs-0">
          <a href="https://docs.google.com/forms/d/e/1FAIpQLScQYn64CDzN_earaxOOOJqmvdsDJM0ORRDTjITQHIJrdAhalg/viewform?pli=1" class="mb-0 btn-block btn-custom">Click here to take the survey</a>
        </div>
        <div class="col-md-6 mb-3 mb-xs-0">
          <a href="https://ccrpc.gitlab.io/transportation-voices-2050/" class="mb-0 btn-block btn-custom">Click here to access the map</a>
        </div>
      </div>
    </section>
    <hr class="future-vision-hr">
    <section class="comment-form">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block far fa-comment icon-size text-danger mr-3" aria-hidden="true"></i>
        <h2 id="general-comment"> Leave a general comment </h2>
      </div>
      <p> If you would like to provide a general comment on the C-U Long Range Transportation Plan, please use this comment box. </p>
      <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdZktUbPHzMR49F-CkQ8nkPU_7nNYXaEHmsw9QwkFNjh6cAmQ/viewform?embedded=true" width="100%" height="768" frameborder="0" marginheight="0" marginwidth="0"> Loading…</iframe>
    </section>
    <hr class="future-vision-hr">
    <section class="event-form">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block far fa-calendar-check icon-size text-danger icon-size mr-3" aria-hidden="true"></i>
        <h2 id="event"> Attend an upcoming event </h2>
      </div>
      <p>
        <strong>Use this form to invite us to your events and meetings.</strong> Please let us know in your message if we can provide accommodations, accessibility information, or materials in alternative formats.
      </p>
      <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdZktUbPHzMR49F-CkQ8nkPU_7nNYXaEHmsw9QwkFNjh6cAmQ/viewform?embedded=true" width="100%" height="768" frameborder="0" marginheight="0" marginwidth="0"> Loading…</iframe>
    </section>
    <hr class="future-vision-hr">
    <section class="survey-form">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fa-solid fa-bullseye icon-size text-danger icon-size mr-3" aria-hidden="true"></i>
        <h2 id="survey">What goals are most important to you?</h2>
      </div>
      <p>We are looking for your input on how much emphasis should be placed on different goals when evaluating and prioritizing potential investments. For example, is it more important to you to improve safety along high-collision corridors, or to support climate goals by making it easier for people to walk, bike, or take transit? Move the sliding circles for each goal to tell us how important that goal is to you.</p>
      <div class="a-component" style="border: 2px solid red">
        <h4 class="custom-heading mb-0 px-3 py-2" style="background-color:red; color:white;">Safety</h4>
        <div class="container mb-0">
          <div class="row mb-0">
            <!-- Left Column (2) -->
            <div class="col-md-2 mb-0 px-0 d-flex" style="background-color: pink;">
              <div class="align-self-center mb-0 ">
                <img src="Map_QR.png" class="mb-0 p-3 img-fluid">
              </div>
            </div>
            <!-- Right Column (10) -->
            <div class="col-md-10 my-4">
              <h5>Everyone feels safe traveling in Seattle, and there are no serious injury or fatal crashes.</h5>
              <input type="range" id="safety" name="safety" min="0" max="10" class="slider">
              <div class="mb-0">
                <span>
                  <b>LEAST IMPORTANT</b>
                </span>
                <span class="float-right mb-0">
                  <b>VERY IMPORTANT</b>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="a-component" style="border: 2px solid green">
        <h4 class="custom-heading mb-0 px-3 py-2" style="background-color:green; color:white;">Equity</h4>
        <div class="container mb-0">
          <div class="row mb-0">
            <!-- Left Column (2) -->
            <div class="col-md-2 mb-0 px-0 d-flex" style="background-color: #d2eedc;">
              <div class="align-self-center mb-0 ">
                <img src="Map_QR.png" class="mb-0 p-3 img-fluid">
              </div>
            </div>
            <!-- Right Column (10) -->
            <div class="col-md-10 my-4">
              <h5>Co-create with community and implement restorative practices to address transportation-related inequities.</h5>
              <input type="range" id="equity" name="equity" min="0" max="10" class="slider">
              <div class="mb-0">
                <span>
                  <b>LEAST IMPORTANT</b>
                </span>
                <span class="float-right mb-0">
                  <b>VERY IMPORTANT</b>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="a-component" style="border: 2px solid goldenrod">
        <h4 class="custom-heading mb-0 px-3 py-2" style="background-color:goldenrod; color:white;">Mobility</h4>
        <div class="container mb-0">
          <div class="row mb-0">
            <!-- Left Column (2) -->
            <div class="col-md-2 mb-0 px-0 d-flex" style="background-color: gold;">
              <div class="align-self-center mb-0 ">
                <img src="Map_QR.png" class="mb-0 p-3 img-fluid">
              </div>
            </div>
            <!-- Right Column (10) -->
            <div class="col-md-10 my-4">
              <h5>Provide reliable and affordable travel options that help people and goods get where they need to go.</h5>
              <input type="range" id="mobility" name="mobility" min="0" max="10" class="slider">
              <div class="mb-0">
                <span>
                  <b>LEAST IMPORTANT</b>
                </span>
                <span class="float-right mb-0">
                  <b>VERY IMPORTANT</b>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button type="submit" class="btn-custom mx-auto w-50" onclick="submitForm()">Submit</button>
    </section>
    <hr class="future-vision-hr">
    <section class="faq">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fas fa-question-circle icon-size text-danger icon-size mr-3" aria-hidden="true"></i>
        <h2 id="faq"> FAQs </h2>
      </div>
      <p> Welcome to our FAQ section! Below, you'll find answers to some of the most commonly asked questions about our products and services. If you can't find the information you're looking for, feel free to reach out to us. We're here to help you! </p>
        {{<accordion border="true" multiselect="false" level="5">}}
  {{%accordion-content title="National Transportation Goals"%}}
  
  **Safety:** To achieve a significant reduction in traffic fatalities and serious injuries on all public roads.
  
  **Infrastructure Condition:** To maintain the highway infrastructure asset system in a state of good repair.
  
  **Congestion Reduction:** To achieve a significant reduction in congestion on the National Highway System.
  
  **System Reliability:** To improve the efficiency of the surface transportation system.
  
  **Freight Movement and Economic Vitality:** To improve the National Highway Freight Network, strengthen the ability of rural communities to access national and international trade markets, and support regional economic development.
  
  **Environmental Sustainability:** To enhance the performance of the transportation system while protecting and enhancing the natural environment.
  
  **Reduced Project Delivery Delays:** To reduce project costs, promote jobs and the economy, and expedite the movement of people and goods by accelerating project completion through eliminating delays in the project development and delivery process, including reducing regulatory burdens and improving agencies' work practices.
  
  _Source: United States Code, 23 U.S.C. 150(b)_
  {{%/accordion-content%}}
  {{%accordion-content title="State Transportation Goals"%}}
  
  **Economy:** Improve Illinois’ economy by providing transportation infrastructure that supports the efficient movement of people and goods.
  
  **Access:** Support all modes of transportation to improve accessibility and safety by improving connections between all modes of transportation.
  
  **Livability:** Enhance quality of life across the state by ensuring that transportation investments advance local goals, provide multimodal options and preserve the environment.
  
  **Stewardship:** Safeguard existing funding the increase revenues to support system maintenance, modernization and strategic growth of Illinois’ transportation system.
  
  **Resilience:** Proactively assess, plan, and invest in the state’s transportation system to ensure that our infrastructure is prepared to sustain and recover from extreme weather events and other disruptions.
  
  _Source: Illinois Department of Transportation, Long Range Transportation Plan, 2019_
  {{%/accordion-content%}}
{{</accordion>}}
    </section>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
      function submitForm() {
        event.preventDefault();
        const safety = document.getElementById('safety').value;
        const equity = document.getElementById('equity').value;
        const mobility = document.getElementById('mobility').value;
        // Define the URL of the API endpoint
        const apiUrl = 'https://forms.ccrpc.org/wioa/lrtp-2050-survey/submission';
        // Define the request headers and body
        const requestOptions = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: {
              safety: safety,
              equity: equity,
              mobility: mobility,
            }
          }),
        };
        // Make the HTTP request
        fetch(apiUrl, requestOptions).then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        }).then(data => {
          // Acknowledge response and reload page to reset sliders
          alert("Thank you for your submisssion!");
          location.reload();
        }).catch(error => {
          // Handle errors, such as network issues or server errors
          console.error('Error:', error);
        });
      }
    </script>
    <style>
      .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        background: #3186b4;
        outline: none;
        -webkit-transition: .2s;
        transition: opacity .2s;
      }
      .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 70px;
        height: 70px;
        background: url("car.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider::-moz-range-thumb {
        width: 70px;
        height: 70px;
        background: url("car.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      /* Style the labels */
      .range-labels {
        display: flex;
        justify-content: space-between;
        margin-top: 10px;
      }
      .range-labels>span,
      p {
        font-size: 1em;
      }
      .custom-heading {
        font-weight: 700;
      }
      @media (max-width: 767.98px) {
        .slider::-webkit-slider-thumb {
          -webkit-appearance: none;
          appearance: none;
          width: 70px;
          height: 70px;
          background: url("car.png");
          background-size: 100% auto;
          cursor: pointer;
        }
        .slider::-moz-range-thumb {
          width: 70px;
          height: 70px;
          background: url("car.png");
          background-size: 100% auto;
          cursor: pointer;
        }
      }
    </style>
  </body>
</html>